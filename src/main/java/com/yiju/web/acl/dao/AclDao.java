//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.dao;

import com.yiju.web.acl.model.SysFunction;
import com.yiju.web.acl.model.UserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AclDao {
    UserRole getRole(Integer var1);

    UserRole getRoleByAlias(String var1);

    Integer getSearchRolesCount(@Param("name") String var1);

    List<UserRole> searchRoles(@Param("name") String var1, @Param("offset") int var2, @Param("pageSize") int var3);

    boolean createRole(UserRole var1);

    boolean updateRole(UserRole var1);

    boolean removeRole(int var1);

    boolean addRoleFunctions(@Param("roleId") int var1, @Param("functions") List<SysFunction> var2);

    boolean removeRoleFunctions(int var1);

    List<SysFunction> getRoleFunctions(int var1);

    List<SysFunction> getAllFunctions();

    List<UserRole> getUserRoles(int var1);

    boolean removeUserRoles(@Param("uid") int var1, @Param("roles") List<Integer> var2);

    boolean addUserRoles(@Param("uid") int var1, @Param("roles") List<UserRole> var2);
}
