//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.service;

import com.yiju.web.acl.model.SysFunction;
import com.yiju.web.acl.model.User;
import com.yiju.web.acl.model.UserRole;

import java.util.List;

public interface AclService {
    UserRole getRole(String var1);

    UserRole getRole(int var1);

    int getSearchRolesCount(String var1);

    List<UserRole> searchRoles(String var1, int var2, int var3);

    boolean createRole(UserRole var1);

    boolean updateRole(UserRole var1);

    boolean removeRole(int var1);

    boolean updateRoleFunctions(int var1, List<SysFunction> var2);

    List<SysFunction> getRoleFunctions(int var1);

    List<SysFunction> getAllFunctions();

    List<UserRole> getUserRoles(int var1);

    boolean removeUserRoles(int var1, List<Integer> var2);

    boolean addUserRoles(int var1, List<UserRole> var2);

    boolean hasRouteRights(User var1, String var2);

    List<UserRole> getUserRouteRoles(User var1, String var2);

    List<SysFunction> getUserMenus(User var1);
}
