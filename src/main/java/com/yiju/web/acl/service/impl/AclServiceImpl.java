//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.service.impl;

import com.yiju.web.acl.dao.AclDao;
import com.yiju.web.acl.model.SysFunction;
import com.yiju.web.acl.model.User;
import com.yiju.web.acl.model.UserRole;
import com.yiju.web.acl.service.AclService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AclServiceImpl implements AclService {
    @Autowired
    private AclDao aclDao;

    public AclServiceImpl() {
    }

    public UserRole getRole(String alias) {
        UserRole role = this.aclDao.getRoleByAlias(alias);
        if (role != null) {
            role.setFunctions(this.aclDao.getRoleFunctions(role.getId().intValue()));
        }

        return role;
    }

    public UserRole getRole(int id) {
        UserRole role = this.aclDao.getRole(Integer.valueOf(id));
        if (role != null) {
            role.setFunctions(this.aclDao.getRoleFunctions(id));
        }

        return role;
    }

    public List<UserRole> searchRoles(String name, int offset, int pageSize) {
        return this.aclDao.searchRoles(name, offset, pageSize);
    }

    public int getSearchRolesCount(String name) {
        return this.aclDao.getSearchRolesCount(name).intValue();
    }

    public boolean createRole(UserRole role) {
        this.aclDao.createRole(role);
        boolean ret = role.getId() != null && role.getId().intValue() > 0;
        if (ret && role.getFunctions() != null) {
            this.aclDao.addRoleFunctions(role.getId().intValue(), role.getFunctions());
        }

        return ret;
    }

    public boolean updateRole(UserRole role) {
        this.aclDao.updateRole(role);
        if (role.getFunctions() != null) {
            this.aclDao.removeRoleFunctions(role.getId().intValue());
            this.aclDao.addRoleFunctions(role.getId().intValue(), role.getFunctions());
        }

        return true;
    }

    public boolean removeRole(int roleId) {
        this.aclDao.removeRole(roleId);
        this.aclDao.removeRoleFunctions(roleId);
        return true;
    }

    public boolean updateRoleFunctions(int roleId, List<SysFunction> functions) {
        this.aclDao.removeRoleFunctions(roleId);
        if (functions != null && functions.size() > 0) {
            this.aclDao.addRoleFunctions(roleId, functions);
        }

        return true;
    }

    public List<UserRole> getUserRoles(int uid) {
        List<UserRole> roles = this.aclDao.getUserRoles(uid);
        return roles;
    }

    public List<SysFunction> getRoleFunctions(int roleId) {
        return this.aclDao.getRoleFunctions(roleId);
    }

    public List<SysFunction> getAllFunctions() {
        return this.aclDao.getAllFunctions();
    }

    public boolean removeUserRoles(int uid, List<Integer> roles) {
        return this.aclDao.removeUserRoles(uid, roles);
    }

    public boolean addUserRoles(int uid, List<UserRole> roles) {
        return this.aclDao.addUserRoles(uid, roles);
    }

    public boolean hasRouteRights(User user, String route) {
        if (user != null && user.getRoles() != null && user.getRoles().size() != 0 && route != null) {
            List<UserRole> roles = user.getRoles();

            for (int i = 0; i < roles.size(); ++i) {
                UserRole role = (UserRole) roles.get(i);
                if (role.isAdministrator()) {
                    return true;
                }

                List<SysFunction> functions = this.getRoleFunctions(role.getId().intValue());

                for (int j = 0; j < functions.size(); ++j) {
                    SysFunction func = (SysFunction) functions.get(j);
                    if (func.getRoute() != null && func.getRoute().equalsIgnoreCase(route)) {
                        return true;
                    }

                    String dependRoutes = func.getDependRoutes();
                    if (dependRoutes != null) {
                        String[] a = dependRoutes.split(",");
                        String[] var11 = a;
                        int var12 = a.length;

                        for (int var13 = 0; var13 < var12; ++var13) {
                            String r = var11[var13];
                            if (r.trim().equals(route)) {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        } else {
            return false;
        }
    }

    public List<UserRole> getUserRouteRoles(User user, String route) {
        if (user != null && user.getRoles() != null && user.getRoles().size() != 0 && route != null) {
            List<UserRole> ret = new ArrayList();
            List<UserRole> roles = user.getRoles();

            for (int i = 0; i < roles.size(); ++i) {
                UserRole role = (UserRole) roles.get(i);
                if (role.isAdministrator()) {
                    ret.add(role);
                } else {
                    List<SysFunction> functions = this.getRoleFunctions(role.getId().intValue());

                    for (int j = 0; j < functions.size(); ++j) {
                        SysFunction func = (SysFunction) functions.get(j);
                        boolean hasRoute = false;
                        if (func.getRoute() != null && func.getRoute().equalsIgnoreCase(route)) {
                            hasRoute = true;
                        } else {
                            String dependRoutes = func.getDependRoutes();
                            if (dependRoutes == null) {
                                continue;
                            }

                            String[] a = dependRoutes.split(",");
                            String[] var13 = a;
                            int var14 = a.length;

                            for (int var15 = 0; var15 < var14; ++var15) {
                                String r = var13[var15];
                                if (r.trim().equals(route)) {
                                    hasRoute = true;
                                    break;
                                }
                            }
                        }

                        if (hasRoute) {
                            ret.add(role);
                            break;
                        }
                    }
                }
            }

            return ret;
        } else {
            return null;
        }
    }

    public List<SysFunction> getUserMenus(User user) {
        List<UserRole> roles = user.getRoles();
        if (roles == null || roles.size() == 0) {
            roles = this.getUserRoles(user.getUid().intValue());
        }

        Map menus = new HashMap();

        for (int i = 0; i < roles.size(); ++i) {
            List<SysFunction> functions = null;
            UserRole role = (UserRole) roles.get(i);
            if (role.isAdministrator()) {
                functions = this.getAllFunctions();
            } else {
                functions = this.getRoleFunctions(role.getId().intValue());
            }

            if (functions != null) {
                for (int j = 0; j < functions.size(); ++j) {
                    SysFunction func = (SysFunction) functions.get(j);
                    boolean flag = false;
                    if ("menu".equals(func.getType()) && !menus.containsKey(func.getId())) {
                        menus.put(func.getId(), func);
                    }
                }
            }
        }

        return new ArrayList(menus.values());
    }
}
