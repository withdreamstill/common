//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yiju.web.model.Model;

import java.util.Iterator;
import java.util.List;

public class User extends Model {
    private static final long serialVersionUID = 9061197524104498714L;
    private Integer uid;
    private List<UserRole> roles;

    public User() {
    }

    public Integer getUid() {
        return this.uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public List<UserRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    public boolean isAdministrator() {
        if (this.roles != null) {
            Iterator var1 = this.roles.iterator();

            while (var1.hasNext()) {
                UserRole role = (UserRole) var1.next();
                if (role.isAdministrator()) {
                    return true;
                }
            }
        }

        return false;
    }
}
