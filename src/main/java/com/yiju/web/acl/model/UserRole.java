//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.model;

import com.yiju.web.model.Model;

import java.util.Date;
import java.util.List;

public class UserRole extends Model {
    private static final long serialVersionUID = -2304780547806316040L;
    private Integer id;
    private String alias;
    private String name;
    private String description;
    private Integer createUid;
    private Date createTime;
    private Integer updateUid;
    private Date updateTime;
    private List<Integer> requestScope;
    private List<Integer> userRequestScope;
    private List<SysFunction> functions;

    public UserRole() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreateUid() {
        return this.createUid;
    }

    public void setCreateUid(Integer createUid) {
        this.createUid = createUid;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateUid() {
        return this.updateUid;
    }

    public void setUpdateUid(Integer updateUid) {
        this.updateUid = updateUid;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<Integer> getRequestScope() {
        return this.requestScope;
    }

    public void setRequestScope(List<Integer> requestScope) {
        this.requestScope = requestScope;
    }

    public List<Integer> getUserRequestScope() {
        return this.userRequestScope;
    }

    public void setUserRequestScope(List<Integer> userRequestScope) {
        this.userRequestScope = userRequestScope;
    }

    public List<SysFunction> getFunctions() {
        return this.functions;
    }

    public void setFunctions(List<SysFunction> functions) {
        this.functions = functions;
    }

    public boolean isAdministrator() {
        return this.alias != null && this.alias.equals("admin");
    }
}
