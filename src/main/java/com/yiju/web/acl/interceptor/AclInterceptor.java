//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.acl.interceptor;

import com.yiju.web.acl.annotation.Auth;
import com.yiju.web.acl.model.User;
import com.yiju.web.acl.service.AclService;
import com.yiju.web.auth.utils.SessionKey;
import com.yiju.web.validation.exception.AclException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AclInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AclService aclService;

    public AclInterceptor() {
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod method = null;
        if (handler instanceof HandlerMethod) {
            method = (HandlerMethod) handler;
        }

        if (method == null) {
            throw new AclException("acl.error", new Object[0]);
        } else {
            Auth auth = (Auth) method.getMethodAnnotation(Auth.class);
            if (auth != null && !auth.acl()) {
                return true;
            } else {
                User user = (User) request.getAttribute(SessionKey.USER);
                if (user == null) {
                    throw new AclException("acl.error", new Object[0]);
                } else {
                    String route = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
                    user.setRoles(this.aclService.getUserRoles(user.getUid().intValue()));
                    boolean isAuth = this.aclService.hasRouteRights(user, route);
                    if (!isAuth) {
                        throw new AclException("acl.error", new Object[0]);
                    } else {
                        return super.preHandle(request, response, handler);
                    }
                }
            }
        }
    }
}
