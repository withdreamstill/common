//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.ArrayList;
import java.util.HashMap;

public class JsonUtils {
    public JsonUtils() {
    }

    public static Object parse(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = null;

        try {
            if (s.startsWith("[")) {
                obj = mapper.readValue(s, ArrayList.class);
            } else if (s.startsWith("{")) {
                obj = mapper.readValue(s, HashMap.class);
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return obj;
    }

    public static String toString(Object obj) {
        String s = null;

        try {
            if (obj != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(SerializationFeature.INDENT_OUTPUT, false);
                mapper.configure(Feature.ESCAPE_NON_ASCII, true);
                s = mapper.writeValueAsString(obj);
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }

        return s;
    }
}
