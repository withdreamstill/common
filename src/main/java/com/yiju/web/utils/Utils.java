//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils;

import java.util.Calendar;
import java.util.Date;

public class Utils {
    public Utils() {
    }

    public static Date setDateTime(Date date, Integer hh, Integer mm, Integer ss, Integer ms) {
        if (date == null) {
            return null;
        } else {
            Calendar instance = Calendar.getInstance();
            instance.setTime(date);
            if (hh != null) {
                instance.set(11, hh.intValue());
            }

            if (hh != null) {
                instance.set(12, mm.intValue());
            }

            if (hh != null) {
                instance.set(13, ss.intValue());
            }

            if (hh != null) {
                instance.set(14, ms.intValue());
            }

            return instance.getTime();
        }
    }

    public static Long getLongTypeEndTimeOfDay(Date date) {
        date = getEndTimeOfDay(date);
        return Long.valueOf(date == null ? 0L : date.getTime());
    }

    public static Date getEndTimeOfDay(Date date) {
        if (date == null) {
            return null;
        } else {
            int hh = 23;
            int mm = 59;
            int ss = 59;
            int ms = 999;
            return setDateTime(date, Integer.valueOf(hh), Integer.valueOf(mm), Integer.valueOf(ss), Integer.valueOf(ms));
        }
    }

    public static Date getBeginTimeOfDay(Date date) {
        if (date == null) {
            return null;
        } else {
            int hh = 0;
            int mm = 0;
            int ss = 0;
            int ms = 0;
            return setDateTime(date, Integer.valueOf(hh), Integer.valueOf(mm), Integer.valueOf(ss), Integer.valueOf(ms));
        }
    }

    public static Integer parseInt(Object s) {
        if (s == null) {
            return null;
        } else {
            try {
                return Integer.valueOf(Integer.parseInt(s.toString()));
            } catch (Exception var2) {
                var2.printStackTrace();
                return null;
            }
        }
    }
}
