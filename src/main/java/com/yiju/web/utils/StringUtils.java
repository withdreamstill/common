//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    public StringUtils() {
    }

    public static Object getJson(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = null;

        try {
            if (s.startsWith("[")) {
                obj = mapper.readValue(s, ArrayList.class);
            } else if (s.startsWith("{")) {
                obj = mapper.readValue(s, HashMap.class);
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return obj;
    }

    public static String formatPlaceholders(String s, Map<String, String> param) {
        String newStr = s;

        try {
            if (param != null) {
                String re = "\\{([^\\{\\}]+)\\}";
                Pattern p = Pattern.compile(re);

                String ph;
                String v;
                for (Matcher m = p.matcher(newStr); m.find(); newStr = newStr.replace(ph, (String) v)) {
                    String k2 = m.group(1) + "";
                    ph = "{" + k2 + "}";
                    v = "";
                    if (param.containsKey(k2)) {
                        Object v1 = param.get(k2);
                        v = v1 == null ? "" : String.valueOf(v1);
                    }
                }
            }
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        return newStr;
    }

    public static boolean isMatch(String regex, String str) {
        Pattern p = Pattern.compile(regex, 8);
        Matcher m = p.matcher(str);
        return m.find();
    }

    public static String getBrowserName(String userAgent) {
        String IE9 = "MSIE 9.0";
        String IE8 = "MSIE 8.0";
        String IE7 = "MSIE 7.0";
        String IE6 = "MSIE 6.0";
        String MAXTHON = "Maxthon";
        String QQ = "QQBrowser";
        String GREEN = "GreenBrowser";
        String SE360 = "360SE";
        String FIREFOX = "Firefox";
        String OPERA = "Opera";
        String CHROME = "Chrome";
        String SAFARI = "Safari";
        String OTHER = "其它";
        return isMatch(OPERA, userAgent) ? OPERA : (isMatch(CHROME, userAgent) ? CHROME : (isMatch(FIREFOX, userAgent) ? FIREFOX : (isMatch(SAFARI, userAgent) ? SAFARI : (isMatch(SE360, userAgent) ? SE360 : (isMatch(GREEN, userAgent) ? GREEN : (isMatch(QQ, userAgent) ? QQ : (isMatch(MAXTHON, userAgent) ? MAXTHON : (isMatch(IE9, userAgent) ? IE9 : (isMatch(IE8, userAgent) ? IE8 : (isMatch(IE7, userAgent) ? IE7 : (isMatch(IE6, userAgent) ? IE6 : OTHER)))))))))));
    }
}
