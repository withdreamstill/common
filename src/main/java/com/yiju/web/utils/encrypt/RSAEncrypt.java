//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils.encrypt;

import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAEncrypt {
    private static RSAEncrypt loginRSAEncrypt = null;
    private RSAPrivateKey privateKey;
    private RSAPublicKey publicKey;
    private static final char[] HEX_CHAR = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public RSAEncrypt() {
    }

    public RSAPrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public RSAPublicKey getPublicKey() {
        return this.publicKey;
    }

    public void genKeyPair() {
        KeyPairGenerator keyPairGen = null;

        try {
            keyPairGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException var3) {
            var3.printStackTrace();
        }

        keyPairGen.initialize(2048, new SecureRandom());
        KeyPair keyPair = keyPairGen.generateKeyPair();
        this.privateKey = (RSAPrivateKey) keyPair.getPrivate();
        this.publicKey = (RSAPublicKey) keyPair.getPublic();
    }

    public void loadPublicKey(InputStream in) throws Exception {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String readLine = null;
            StringBuilder sb = new StringBuilder();

            while ((readLine = br.readLine()) != null) {
                if (readLine.charAt(0) != 45) {
                    sb.append(readLine);
                    sb.append('\r');
                }
            }

            this.loadPublicKey(sb.toString());
        } catch (IOException var5) {
            throw new Exception("公钥数据流读取错误");
        } catch (NullPointerException var6) {
            throw new Exception("公钥输入流为空");
        }
    }

    public void loadPublicKey(String publicKeyStr) throws Exception {
        try {
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] buffer = base64Decoder.decodeBuffer(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            this.publicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException var6) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException var7) {
            throw new Exception("公钥非法");
        } catch (IOException var8) {
            throw new Exception("公钥数据内容读取错误");
        } catch (NullPointerException var9) {
            throw new Exception("公钥数据为空");
        }
    }

    public void loadPrivateKey(InputStream in) throws Exception {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String readLine = null;
            StringBuilder sb = new StringBuilder();

            while ((readLine = br.readLine()) != null) {
                if (readLine.charAt(0) != 45) {
                    sb.append(readLine);
                    sb.append('\r');
                }
            }

            this.loadPrivateKey(sb.toString());
        } catch (IOException var5) {
            var5.printStackTrace();
            throw new Exception("私钥数据读取错误");
        } catch (NullPointerException var6) {
            var6.printStackTrace();
            throw new Exception("私钥输入流为空");
        }
    }

    public void loadPrivateKey(String privateKeyStr) throws Exception {
        try {
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] buffer = base64Decoder.decodeBuffer(privateKeyStr);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            this.privateKey = (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException var6) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException var7) {
            throw new Exception("私钥非法");
        } catch (IOException var8) {
            throw new Exception("私钥数据内容读取错误");
        } catch (NullPointerException var9) {
            throw new Exception("私钥数据为空");
        }
    }

    public byte[] encrypt(RSAPublicKey publicKey, byte[] plainTextData) throws Exception {
        if (publicKey == null) {
            throw new Exception("加密公钥为空, 请设置");
        } else {
            Cipher cipher = null;

            try {
                cipher = Cipher.getInstance("RSA", new BouncyCastleProvider());
                cipher.init(1, publicKey);
                byte[] output = cipher.doFinal(plainTextData);
                return output;
            } catch (NoSuchAlgorithmException var5) {
                throw new Exception("无此加密算法");
            } catch (NoSuchPaddingException var6) {
                var6.printStackTrace();
                return null;
            } catch (InvalidKeyException var7) {
                throw new Exception("加密公钥非法,请检查");
            } catch (IllegalBlockSizeException var8) {
                throw new Exception("明文长度非法");
            } catch (BadPaddingException var9) {
                throw new Exception("明文数据已损坏");
            }
        }
    }

    public byte[] decrypt(RSAPrivateKey privateKey, byte[] cipherData) throws Exception {
        if (privateKey == null) {
            throw new Exception("解密私钥为空, 请设置");
        } else {
            Cipher cipher = null;

            try {
                cipher = Cipher.getInstance("RSA", new BouncyCastleProvider());
                cipher.init(2, privateKey);
                byte[] output = cipher.doFinal(cipherData);
                return output;
            } catch (NoSuchAlgorithmException var5) {
                throw new Exception("无此解密算法");
            } catch (NoSuchPaddingException var6) {
                var6.printStackTrace();
                return null;
            } catch (InvalidKeyException var7) {
                throw new Exception("解密私钥非法,请检查");
            } catch (IllegalBlockSizeException var8) {
                throw new Exception("密文长度非法");
            } catch (BadPaddingException var9) {
                throw new Exception("密文数据已损坏");
            }
        }
    }

    public static String byteArrayToString(byte[] data) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < data.length; ++i) {
            stringBuilder.append(HEX_CHAR[(data[i] & 240) >>> 4]);
            stringBuilder.append(HEX_CHAR[data[i] & 15]);
            if (i < data.length - 1) {
                ;
            }
        }

        return stringBuilder.toString();
    }

    public static byte[] stringToByteArray(String str) {
        byte[] byteArray = new byte[str.length() / 2];
        int k = 0;

        for (int i = 0; i < byteArray.length; ++i) {
            byte high = (byte) (Character.digit(str.charAt(k), 16) & 255);
            byte low = (byte) (Character.digit(str.charAt(k + 1), 16) & 255);
            byteArray[i] = (byte) (high << 4 | low);
            k += 2;
        }

        return byteArray;
    }

    public static RSAEncrypt getLoginRSAEncrypt() {
        if (loginRSAEncrypt == null) {
            loginRSAEncrypt = new RSAEncrypt();

            try {
                loginRSAEncrypt.loadPublicKey(RSAEncrypt.class.getClassLoader().getResourceAsStream("conf/login_rsa.pub"));
                System.out.println("加载公钥成功");
            } catch (Exception var2) {
                System.err.println(var2.getMessage());
                System.err.println("加载公钥失败");
            }

            try {
                loginRSAEncrypt.loadPrivateKey(RSAEncrypt.class.getClassLoader().getResourceAsStream("conf/login_rsa.key"));
                System.out.println("加载私钥成功");
            } catch (Exception var1) {
                System.err.println(var1.getMessage());
                System.err.println("加载私钥失败");
            }
        }

        return loginRSAEncrypt;
    }

    public String encryptString(String str) {
        String encryStr = null;

        try {
            byte[] cipher = this.encrypt(this.getPublicKey(), str.getBytes("UTF-8"));
            encryStr = (new BASE64Encoder()).encodeBuffer(cipher);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return encryStr;
    }

    public String decryptString(String encryStr) {
        String str = null;

        try {
            byte[] cipher = (new BASE64Decoder()).decodeBuffer(encryStr);
            byte[] txt = this.decrypt(this.getPrivateKey(), cipher);
            str = new String(txt, "UTF-8");
        } catch (Exception var5) {
            var5.printStackTrace();
        }

        return str;
    }

    public static void setLoginRSAEncrypt(RSAEncrypt loginRSAEncrypt) {
        loginRSAEncrypt = loginRSAEncrypt;
    }

    public static void main(String[] args) {
        RSAEncrypt rsaEncrypt = getLoginRSAEncrypt();
        String username = "15801207618";
        String pwd = DigestUtils.md5Hex("1234567890123456");
        String str = username + ":" + pwd + ":abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
        String source = str;

        try {
            System.out.println("source:" + source);
            String encryStr = rsaEncrypt.encryptString(source);
            System.out.println("加密后:" + encryStr);
            encryStr = "jIGd91YxnfxpxgyTAXq2cU/Rhaq1lRzGkdkchMeqAVmOc9I24UAuoiLjoBMDGq+s8lMz2M4Z3e154H4W1HGD0Pcuc7bCEIek3zt6RJdOFuACSn6NhDB1btTbtdWPn5Om8CioinGEPEulH0mwNcVTbWTP4brqqe6dzM/wndKNBWU=";
            String decryStr = rsaEncrypt.decryptString(encryStr);
            System.out.println("解密后：" + decryStr);
        } catch (Exception var8) {
            System.err.println(var8.getMessage());
        }

    }
}
