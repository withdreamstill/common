//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils.encrypt;

import com.yiju.web.utils.Utils;

public class NumberEncrypt {
    private static String VER_01 = "01";

    public NumberEncrypt() {
    }

    private static String encrypt01(String c) {
        if (c == null) {
            return c;
        } else {
            StringBuffer s = new StringBuffer();
            s.append(VER_01);
            String salt = "";

            for (int i = 1; i < c.length(); i += 2) {
                salt = salt + c.substring(i, i + 1);
            }

            String encryptStr = AesUtils.encryptString(c, salt).toUpperCase();
            String sLenSalt = "00" + salt.length();
            s.append(sLenSalt.substring(sLenSalt.length() - 2));
            StringBuffer ret = new StringBuffer();

            for (int i = 0; i < encryptStr.length(); ++i) {
                if (i < salt.length()) {
                    ret.append(salt.substring(i, i + 1));
                }

                ret.append(encryptStr.substring(i, i + 1));
            }

            String sLenEncryptStr = "0000" + ret.length();
            s.append(sLenEncryptStr.substring(sLenEncryptStr.length() - 4));
            s.append(ret);
            return s.toString();
        }
    }

    private static String decrypt01(String s) {
        if (s == null) {
            return s;
        } else {
            int i = 0;
            String ver = s.substring(i, VER_01.length());
            if (!VER_01.equals(ver)) {
                return s;
            } else {
                i = i + ver.length();
                int nSalt = Utils.parseInt(s.substring(i, i + 2)).intValue();
                i += 2;
                int nEncryptStr = Utils.parseInt(s.substring(i, i + 4)).intValue();
                i += 4;
                String encryptStr = s.substring(i);
                if (nEncryptStr != encryptStr.length()) {
                    return s;
                } else {
                    String salt = "";
                    String tmp = "";

                    for (int j = 0; j < nSalt; ++j) {
                        salt = salt + encryptStr.substring(j * 2, j * 2 + 1);
                        tmp = tmp + encryptStr.substring(j * 2 + 1, j * 2 + 2);
                    }

                    tmp = tmp + encryptStr.substring(nSalt * 2);
                    String text = AesUtils.decryptString(tmp, salt);
                    return text;
                }
            }
        }
    }

    public static String decrypt(String s) {
        return decrypt01(s);
    }

    public static String encrypt(String c) {
        return encrypt01(c);
    }

    public static void main(String[] args) {
        String c = "aaa12313621189657";
        String s = encrypt(c);
        System.out.println(c);
        System.out.println(s);
        System.out.println(decrypt(s));
        System.out.println("0105003738251D9B534DFF9C0F24110CA22AB20F42800".equalsIgnoreCase(s));
        System.out.println("123:" + encrypt("123"));
        System.out.println(":" + encrypt(""));
        System.out.println("11:" + decrypt("010000320143DB63EE66B0CDFF9F69917680151E"));
        System.out.println("!@#$%:" + encrypt("18503889415"));
        System.out.println("!@#$%:" + decrypt("01020034@5$8FCC9D04F8BC53866B45E3471239706"));
        System.out.println("!@#$%:" + decrypt("01050037880D8C9112B48BDCA281749233600455C979B"));
    }
}
