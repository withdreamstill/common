//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class YiJuEncrypt {
    public static final String VER_AA = "AA";
    public static final String VER_AB = "AB";
    public static final String VER_AC = "AC";
    public static final String VER_AD = "AD";
    public static final String MAP_KEY_VER = "ver";
    public static final String MAP_KEY_STR = "s";
    public static final String MAP_KEY_ORI = "ori";
    public static final String MAP_KEY_SALT = "salt";
    public static final String MAP_KEY_CONTENT = "content";
    public static final String MAP_KEY_ENCRYPT_STR_LEN = "len";
    public static final String MAP_KEY_CHECK_CODE = "checkCode";
    public static final String MAP_KEY_TIME = "time";
    private static int DATA_FLAG_LEN = 4;
    private static int SALT_LEN = 8;
    private static int TIME_LEN = 10;
    private static int CHECK_CODE_LEN = 8;

    public YiJuEncrypt() {
    }

    private static Map parseAA(String s) {
        try {
            Map ret = new HashMap();
            ret.put("s", s);
            int i = 0;
            String ver = s.substring(i, "AA".length());
            if (ver != null && "AA".equalsIgnoreCase(ver)) {
                ret.put("ver", ver);
                i = i + "AA".length();
                int nLen = Integer.parseInt(s.substring(i, i + DATA_FLAG_LEN), 16);
                ret.put("len", Integer.valueOf(nLen));
                i += DATA_FLAG_LEN;
                String salt = s.substring(i, i + SALT_LEN);
                if (salt == null) {
                    return null;
                } else {
                    ret.put("salt", salt);
                    i += SALT_LEN;
                    String encryptString = s.substring(i, i + nLen - SALT_LEN);
                    if (encryptString == null) {
                        return null;
                    } else {
                        i += nLen - SALT_LEN;
                        if (i + 1 >= s.length()) {
                            return null;
                        } else {
                            String pwd = DigestUtils.sha1Hex(salt);
                            String oriString = AesUtils.decryptString(encryptString, pwd);
                            if (oriString == null) {
                                return null;
                            } else {
                                ret.put("ori", oriString);
                                String checkCode = s.substring(i);
                                String checkCode2 = DigestUtils.sha1Hex(oriString).substring(0, checkCode.length());
                                if (checkCode.equalsIgnoreCase(checkCode2)) {
                                    ret.put("content", oriString);
                                    return ret;
                                } else {
                                    return null;
                                }
                            }
                        }
                    }
                }
            } else {
                return null;
            }
        } catch (Exception var11) {
            var11.printStackTrace();
            return null;
        }
    }

    private static String encryptAA(String c) {
        StringBuffer s = new StringBuffer();
        s.append("AA");
        String salt = DigestUtils.sha1Hex(System.nanoTime() + "").substring(0, SALT_LEN).toUpperCase();
        String pwd = DigestUtils.sha1Hex(salt);
        String encryptString = AesUtils.encryptString(c, pwd);
        int len = salt.length() + encryptString.length();
        String sLen = "0000" + Integer.toHexString(len);
        sLen = sLen.substring(sLen.length() - DATA_FLAG_LEN);
        String checkCode = DigestUtils.sha1Hex(c).substring(0, CHECK_CODE_LEN);
        s.append(sLen);
        s.append(salt);
        s.append(encryptString);
        s.append(checkCode);
        return s.toString().toUpperCase();
    }

    private static String encryptAB(String c) {
        StringBuffer sb = new StringBuffer();
        sb.append("AB");
        String t = (System.currentTimeMillis() + "").substring(0, 10);
        String salt = DigestUtils.sha1Hex(System.nanoTime() + "").substring(0, SALT_LEN).toUpperCase();
        String pwd = DigestUtils.sha1Hex(salt);
        String oriString = t + c;
        String encryptString = AesUtils.encryptString(oriString, pwd);
        int len = salt.length() + encryptString.length();
        String sLen = "0000" + Integer.toHexString(len);
        sLen = sLen.substring(sLen.length() - DATA_FLAG_LEN);
        String checkCode = DigestUtils.sha1Hex(oriString).substring(0, CHECK_CODE_LEN);
        sb.append(sLen);
        sb.append(salt);
        sb.append(encryptString);
        sb.append(checkCode);
        return sb.toString().toUpperCase();
    }

    private static Map parseAB(String s) {
        try {
            Map ret = new HashMap();
            ret.put("s", s);
            int i = 0;
            String ver = s.substring(i, "AB".length());
            if (ver != null && "AB".equalsIgnoreCase(ver)) {
                ret.put("ver", ver);
                i = i + "AB".length();
                int nLen = Integer.parseInt(s.substring(i, i + DATA_FLAG_LEN), 16);
                ret.put("len", Integer.valueOf(nLen));
                i += DATA_FLAG_LEN;
                String salt = s.substring(i, i + SALT_LEN);
                if (salt == null) {
                    return null;
                } else {
                    ret.put("salt", salt);
                    i += SALT_LEN;
                    String encryptString = s.substring(i, i + nLen - SALT_LEN);
                    if (encryptString == null) {
                        return null;
                    } else {
                        i += nLen - SALT_LEN;
                        if (i + 1 >= s.length()) {
                            return null;
                        } else {
                            String pwd = DigestUtils.sha1Hex(salt);
                            String oriString = AesUtils.decryptString(encryptString, pwd);
                            if (oriString == null) {
                                return null;
                            } else {
                                ret.put("ori", oriString);
                                String checkCode = s.substring(i);
                                ret.put("checkCode", checkCode);
                                String checkCode2 = DigestUtils.sha1Hex(oriString).substring(0, checkCode.length());
                                if (checkCode.equalsIgnoreCase(checkCode2)) {
                                    String t = oriString.substring(0, TIME_LEN);
                                    String c = oriString.substring(TIME_LEN);
                                    ret.put("time", t);
                                    ret.put("content", c);
                                    return ret;
                                } else {
                                    return null;
                                }
                            }
                        }
                    }
                }
            } else {
                return null;
            }
        } catch (Exception var13) {
            var13.printStackTrace();
            return null;
        }
    }

    private static String encryptAC(String c) {
        int dataLenFlag = 3;
        StringBuffer sb = new StringBuffer();
        sb.append("AC");
        String t = (System.currentTimeMillis() + "").substring(0, 10);
        String salt = UUID.randomUUID().toString().substring(0, SALT_LEN);
        String pwd = DigestUtils.sha1Hex(salt);
        String oriString = t + c;
        String encryptString = AesUtils.encryptString(oriString, pwd);
        int len = salt.length() + encryptString.length();
        String sLen = "0000" + Integer.toString(len, 36);
        sLen = sLen.substring(sLen.length() - dataLenFlag);
        String checkCode = DigestUtils.sha1Hex(oriString).substring(0, CHECK_CODE_LEN);
        sb.append(sLen);
        sb.append(salt);
        sb.append(encryptString);
        sb.append(checkCode);
        return sb.toString();
    }

    private static Map parseAC(String s) {
        try {
            int dataLenFlag = 3;
            Map ret = new HashMap();
            ret.put("s", s);
            int i = 0;
            String ver = s.substring(i, "AC".length());
            if (ver != null && "AC".equalsIgnoreCase(ver)) {
                ret.put("ver", ver);
                i = i + "AC".length();
                int nLen = Integer.parseInt(s.substring(i, i + dataLenFlag), 36);
                ret.put("len", Integer.valueOf(nLen));
                i += dataLenFlag;
                String salt = s.substring(i, i + SALT_LEN);
                if (salt == null) {
                    return null;
                } else {
                    ret.put("salt", salt);
                    i += SALT_LEN;
                    String encryptString = s.substring(i, i + nLen - SALT_LEN);
                    if (encryptString == null) {
                        return null;
                    } else {
                        i += nLen - SALT_LEN;
                        if (i + 1 >= s.length()) {
                            return null;
                        } else {
                            String pwd = DigestUtils.sha1Hex(salt);
                            String oriString = AesUtils.decryptString(encryptString, pwd);
                            if (oriString == null) {
                                return null;
                            } else {
                                ret.put("ori", oriString);
                                String checkCode = s.substring(i);
                                ret.put("checkCode", checkCode);
                                String checkCode2 = DigestUtils.sha1Hex(oriString).substring(0, checkCode.length());
                                if (checkCode.equalsIgnoreCase(checkCode2)) {
                                    String t = oriString.substring(0, TIME_LEN);
                                    String c = oriString.substring(TIME_LEN);
                                    ret.put("time", t);
                                    ret.put("content", c);
                                    return ret;
                                } else {
                                    return null;
                                }
                            }
                        }
                    }
                }
            } else {
                return null;
            }
        } catch (Exception var14) {
            var14.printStackTrace();
            return null;
        }
    }

    private static String encryptAD(String c) {
        int dataFlagLen = 2;
        StringBuffer sb = new StringBuffer();
        sb.append("AD");
        String t = Long.toString(System.currentTimeMillis(), 36);
        String uuid = DigestUtils.sha1Hex(UUID.randomUUID().toString());
        String cc = (new StringBuffer(t + uuid)).reverse().toString();
        String checkCode = DigestUtils.sha256Hex(DigestUtils.sha1Hex(cc)).substring(0, CHECK_CODE_LEN);
        int len = (t + uuid + checkCode).length();
        String sLen = "00" + Integer.toString(len, 36);
        sLen = sLen.substring(sLen.length() - dataFlagLen);
        sb.append(sLen);
        sb.append(t);
        sb.append(uuid);
        sb.append(checkCode);
        return sb.toString();
    }

    private static Map parseAD(String s) {
        try {
            int dataFlagLen = 2;
            Map ret = new HashMap();
            ret.put("s", s);
            int i = 0;
            String ver = s.substring(i, "AD".length());
            if (ver != null && "AD".equalsIgnoreCase(ver)) {
                ret.put("ver", ver);
                i = i + "AD".length();
                int nLen = Integer.parseInt(s.substring(i, i + dataFlagLen), 36);
                ret.put("len", Integer.valueOf(nLen));
                i += dataFlagLen;
                String ts = s.substring(i, i + nLen - 48);
                Long t = Long.valueOf(Long.parseLong(ts, 36));
                ret.put("time", t);
                String c = s.substring(s.length() - 48, s.length() - 8);
                ret.put("content", c);
                String checkCode = s.substring(s.length() - 8);
                String cc = (new StringBuffer(ts + c)).reverse().toString();
                String checkCode2 = DigestUtils.sha256Hex(DigestUtils.sha1Hex(cc)).substring(0, CHECK_CODE_LEN);
                return checkCode.equals(checkCode2) ? ret : null;
            } else {
                return null;
            }
        } catch (Exception var12) {
            var12.printStackTrace();
            return null;
        }
    }

    public static String encrypt(String c, String ver) {
        byte var3 = -1;
        switch (ver.hashCode()) {
            case 2080:
                if (ver.equals("AA")) {
                    var3 = 0;
                }
                break;
            case 2081:
                if (ver.equals("AB")) {
                    var3 = 1;
                }
                break;
            case 2082:
                if (ver.equals("AC")) {
                    var3 = 2;
                }
                break;
            case 2083:
                if (ver.equals("AD")) {
                    var3 = 3;
                }
        }

        switch (var3) {
            case 0:
                return encryptAA(c);
            case 1:
                return encryptAB(c);
            case 2:
                return encryptAC(c);
            case 3:
                return encryptAD(c);
            default:
                return null;
        }
    }

    public static String encrypt(String c) {
        return encrypt(c, "AA");
    }

    public static Map parse(String s) {
        String ver = s.substring(0, 2);
        Map ret = null;
        byte var4 = -1;
        switch (ver.hashCode()) {
            case 2080:
                if (ver.equals("AA")) {
                    var4 = 0;
                }
                break;
            case 2081:
                if (ver.equals("AB")) {
                    var4 = 1;
                }
                break;
            case 2082:
                if (ver.equals("AC")) {
                    var4 = 2;
                }
                break;
            case 2083:
                if (ver.equals("AD")) {
                    var4 = 3;
                }
        }

        switch (var4) {
            case 0:
                ret = parseAA(s);
                break;
            case 1:
                ret = parseAB(s);
                break;
            case 2:
                ret = parseAC(s);
                break;
            case 3:
                ret = parseAD(s);
                break;
            default:
                ret = null;
        }

        return ret;
    }

    public static String decrypt(String s) {
        Map ret = parse(s);
        if (ret != null && ret.containsKey("content")) {
            String c = (String) ret.get("content");
            return c;
        } else {
            return null;
        }
    }

    public static String decrypt(String s, String ver) {
        Map ret = parse(s);
        if (ret != null && ret.containsKey("content")) {
            String ver2 = (String) ret.get("ver");
            if (ver != null && !ver.equalsIgnoreCase(ver2)) {
                return null;
            } else {
                String c = (String) ret.get("content");
                return c;
            }
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(parseAB("AB002885818FBC6B2F82EA3B23455B9E757B426F58AFBB4AFE4A3D"));
        String c = encrypt("　　新华社北京9月27日电 中共中央总书记、国家主席、中央军委主席习近平日前作出重要指示强调，精神文明建设“五个一工程”实施20多年来，以弘扬先进文化、多出优秀作品为目标，推出一大批思想精深、艺术精湛、制作精良的作品，成为精神文化产品创作生产的示范工程、响亮品牌，丰富了人民精神文化生活，发挥了以优秀的作品鼓舞人的重要作用。", "AA");
        c = encrypt("0huaxin,xiaozhu2017-09-01 00:00:002017-09-01 23:59:59", "AB");
        String s = decrypt(c + "1");
        s = Integer.toString(32, 36);
        System.out.println("s:" + s);
        s = encryptAC("xxx");
        System.out.println("e ac:" + s);
        s = "AC02W0A620F734E5174204F3249E32EACC0B00DEFB19DCCE39C8AC8C89BDF4E0A318203B2CFEA460D8F5E96598120FE0CE855A3B691BCED3F23BA";
        System.out.println("parse ac:" + parse(s));
        s = encryptAD("xxx");
        System.out.println("e ad:" + s);
        System.out.println("parse ad:" + parse(s));
    }
}
