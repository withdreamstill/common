//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageUtils {
    public ImageUtils() {
    }

    public static boolean createThumbnail(String fp, String destFp, String destExt, int destWidth, int destHeight) {
        File f = new File(fp);
        BufferedImage buffer = null;

        try {
            buffer = ImageIO.read(f);
        } catch (IOException var19) {
            var19.printStackTrace();
            return false;
        }

        int w = buffer.getWidth();
        int h = buffer.getHeight();
        double ratioX = (double) destWidth / (double) w;
        double ratioY = (double) destHeight / (double) h;
        double ratio = ratioX > ratioY ? ratioX : ratioY;
        AffineTransform tf = AffineTransform.getScaleInstance(ratio, ratio);
        AffineTransformOp op = new AffineTransformOp(tf, (RenderingHints) null);
        buffer = op.filter(buffer, (BufferedImage) null);
        buffer = buffer.getSubimage((buffer.getWidth() - destWidth) / 2, (buffer.getHeight() - destHeight) / 2, destWidth, destHeight);

        try {
            File destFile = new File(destFp);
            if (destExt == null || destExt.equals("")) {
                int i = destFp.lastIndexOf(".");
                if (i > 0 && i + 1 < destFp.length()) {
                    destExt = destFp.substring(i + 1);
                }
            }

            if (destExt == null || destExt.equals("")) {
                destExt = "jpg";
            }

            ImageIO.write(buffer, destExt, destFile);
            return true;
        } catch (Exception var20) {
            var20.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) {
        createThumbnail("/Users/baboy/Desktop/test.jpg", "/Users/baboy/Desktop/test.copy.jpg", (String) null, 100, 190);
    }
}
