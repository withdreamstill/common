//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.dao.sys;

import com.yiju.web.model.sys.SysConf;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysConfDao {
    Integer addConf(SysConf var1);

    boolean removeConf(Integer var1);

    boolean updateConf(SysConf var1);

    List<SysConf> getAllConfs();

    SysConf getConf(Integer var1);

    SysConf getConfByKey(String var1);
}
