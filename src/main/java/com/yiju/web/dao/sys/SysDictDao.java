//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.dao.sys;

import com.yiju.web.model.sys.SysDict;
import com.yiju.web.model.sys.SysDict.SysDictItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysDictDao {
    Integer addDict(SysDict var1);

    boolean updateDict(SysDict var1);

    boolean addDictItems(@Param("dictId") Integer var1, @Param("items") List<SysDictItem> var2);

    boolean removeItems(Integer var1);

    List<SysDict> getAllDicts();

    SysDict getDict(Integer var1);

    SysDict getDictByAlias(String var1);
}
