//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class AuthException extends HandleException {
    private static final long serialVersionUID = -613749799428461420L;

    public AuthException(String msgKey, Object... args) {
        super(StatusCode.ERROR_AUTH, msgKey, args);
    }

    public AuthException() {
        super(StatusCode.ERROR_AUTH, "auth.error", new Object[0]);
    }
}
