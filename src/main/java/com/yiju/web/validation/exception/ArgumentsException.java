//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class ArgumentsException extends HandleException {
    private static final long serialVersionUID = -613749799428461420L;

    public ArgumentsException(String msgKey, Object... args) {
        super(StatusCode.ERROR_PARAM, msgKey, args);
    }

    public ArgumentsException() {
        super(StatusCode.ERROR_PARAM, "param.error", new Object[0]);
    }
}
