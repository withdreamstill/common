//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.utils.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HandleException extends RuntimeException {
    private static final long serialVersionUID = -8997168517216976570L;
    private static final Logger log = LoggerFactory.getLogger(HandleException.class);
    private int status = 0;

    public HandleException(int status, String msgKey, Object... args) {
        super(MessageSource.getMessage(msgKey, args));
        String declaringClass = null;
        int lineNumber = 0;
        String methodName = null;
        StackTraceElement[] tes = this.getStackTrace();
        if (tes != null && tes.length > 0) {
            StackTraceElement te = tes[0];
            declaringClass = te.getClassName();
            lineNumber = te.getLineNumber();
            methodName = te.getMethodName();
        }

        log.error("HandleException|class:" + declaringClass + "|method:" + methodName + "|line:" + lineNumber + "|msg:" + msgKey + "|status:" + status);
        this.status = status;
    }

    public String getErrorMessage() {
        return super.getLocalizedMessage();
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
