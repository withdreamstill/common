//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class WorkflowException extends HandleException {
    private static final long serialVersionUID = -4680015994220326403L;

    public WorkflowException(String msgKey, Object... args) {
        super(StatusCode.FAILURE, msgKey, args);
    }

    public WorkflowException(int statusCode, String msgKey, Object... args) {
        super(statusCode, msgKey, args);
    }
}
