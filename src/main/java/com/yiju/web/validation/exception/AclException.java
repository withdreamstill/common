//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class AclException extends HandleException {
    private static final long serialVersionUID = -613749799428461420L;

    public AclException(String msgKey, Object... args) {
        super(StatusCode.ERROR_ACL, msgKey, args);
    }

    public AclException() {
        super(StatusCode.ERROR_ACL, "acl.error", new Object[0]);
    }
}
