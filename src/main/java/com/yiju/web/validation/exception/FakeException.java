//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class FakeException extends HandleException {
    public FakeException(String msgKey, Object... args) {
        super(StatusCode.SUCCESS, msgKey, args);
    }

    public FakeException() {
        super(StatusCode.SUCCESS, "request.success", new Object[0]);
    }

    public FakeException(int statusCode, String msgKey, Object... args) {
        super(statusCode, msgKey, args);
    }
}
