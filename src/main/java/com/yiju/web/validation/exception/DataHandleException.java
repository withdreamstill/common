//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.validation.exception;

import com.yiju.web.http.StatusCode;

public class DataHandleException extends HandleException {
    private static final long serialVersionUID = 5034570055721040440L;

    public DataHandleException(int status, String msgKey, Object... args) {
        super(status, msgKey, args);
    }

    public DataHandleException(String msgKey, Object... args) {
        super(StatusCode.ERROR, msgKey, args);
    }

    public DataHandleException() {
        super(StatusCode.ERROR, "data.handle.error", new Object[0]);
    }
}
