//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service;

import com.yiju.web.model.sys.SysConf;

import java.util.List;

public interface SysConfService {
    Integer addConf(SysConf var1);

    boolean removeConf(Integer var1);

    boolean updateConf(SysConf var1);

    List<SysConf> getAllConfs();

    List<SysConf> getAllConfs(String var1);

    SysConf getConf(Integer var1);

    SysConf getConfByKey(String var1);
}
