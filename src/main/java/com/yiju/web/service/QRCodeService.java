//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service;

import java.awt.image.BufferedImage;

public interface QRCodeService {
    BufferedImage createQRCode(String var1, int var2, int var3);

    boolean saveImage(BufferedImage var1, String var2);

    boolean createQRCode(String var1, int var2, int var3, String var4);

    byte[] getBytes(BufferedImage var1);

    void parseQRCode();
}
