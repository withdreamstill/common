//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service;

import java.util.Date;

public interface CacheService {
    void set(Object var1, Object var2);

    void set(Object var1, Object var2, int var3);

    void set(Object var1, Object var2, Date var3);

    void remove(Object var1);

    Object get(Object var1);
}
