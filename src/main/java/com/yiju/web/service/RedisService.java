//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service;

import redis.clients.util.Pool;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RedisService {
    boolean setValue(String var1, String var2);

    boolean setValue(String var1, int var2);

    boolean setValue(String var1, int var2, int var3);

    boolean setValue(String var1, String var2, int var3);

    String getValue(String var1);

    int getInt(String var1);

    String lpop(String var1);

    String rpop(String var1);

    boolean lpush(String var1, String var2);

    boolean rpush(String var1, String var2);

    List<String> getList(String var1);

    boolean del(String var1);

    boolean incr(String var1);

    boolean hdel(String var1, String... var2);

    boolean hincrby(String var1, String var2, long var3);

    boolean hincr(String var1, String var2);

    Map hgetAll(String var1);

    Pool getPool();

    boolean sadd(String var1, String... var2);

    Long srem(String var1, String... var2);

    Set<String> sdiff(String... var1);

    Long sdiffstore(String var1, String... var2);

    Long scard(String var1);

    List<String> srandmember(String var1, Integer var2);

    String srandmember(String var1);

    Set<String> smembers(String var1);

    boolean setnx(String var1, String var2, int var3);

    boolean tryLock(String var1, int var2, int var3);

    boolean tryLock(String var1, int var2);

    boolean unLock(String var1);

    boolean execInRedisCli(String var1, String var2);

    boolean execInRedisCli(String var1);
}
