//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import com.yiju.web.service.CacheService;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class MemoryCacheServiceImpl implements CacheService {
    public static String DEFAULT_NM = "g";
    public static Map<Object, HashMap<Object, MemoryCacheServiceImpl.CacheItem>> storage = new HashMap();

    public MemoryCacheServiceImpl() {
    }

    public void set(Object k, Object v) {
        this.set(k, v, (Date) null);
    }

    public void set(Object k, Object v, int expire) {
        Date expireDate = null;
        if (expire > 0) {
            Date now = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(13, expire);
            expireDate = calendar.getTime();
        }

        this.set(k, v, expireDate);
    }

    public void set(Object k, Object v, Date expire) {
        this.set(DEFAULT_NM, k, v, expire);
    }

    public synchronized void set(Object nm, Object k, Object v, Date expire) {
        MemoryCacheServiceImpl.CacheItem item = new MemoryCacheServiceImpl.CacheItem(v, expire);
        HashMap<Object, MemoryCacheServiceImpl.CacheItem> d = (HashMap) storage.get(nm);
        if (d == null) {
            d = new HashMap();
            storage.put(nm, d);
        }

        d.put(k, item);
    }

    public void remove(Object k) {
        this.remove(DEFAULT_NM, k);
    }

    public synchronized void remove(Object nm, Object k) {
        HashMap<Object, MemoryCacheServiceImpl.CacheItem> d = (HashMap) storage.get(nm);
        if (d != null) {
            d.remove(k);
        }

    }

    public Object get(Object k) {
        return this.get(DEFAULT_NM, k);
    }

    public synchronized Object get(Object nm, Object k) {
        HashMap<Object, MemoryCacheServiceImpl.CacheItem> d = (HashMap) storage.get(nm);
        if (d != null) {
            MemoryCacheServiceImpl.CacheItem item = (MemoryCacheServiceImpl.CacheItem) d.get(k);
            if (item != null && (item.getExpireDate() == null || item.getExpireDate().after(new Date()))) {
                return item.getData();
            }

            if (item != null) {
                this.remove(nm, k);
            }
        }

        return null;
    }

    public static class CacheItem {
        private Object data;
        private Date expireDate;

        public CacheItem(Object data, Date expireData) {
            this.data = data;
            this.expireDate = expireData;
        }

        public Object getData() {
            return this.data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Date getExpireDate() {
            return this.expireDate;
        }

        public void setExpireDate(Date expireDate) {
            this.expireDate = expireDate;
        }
    }
}
