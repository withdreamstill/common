//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import java.util.Date;

public class CacheItem {
  private Object data;
  private Date expireDate;

  public CacheItem(Object data, Date expireData) {
    this.data = data;
    this.expireDate = expireData;
  }

  public Object getData() {
    return this.data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public Date getExpireDate() {
    return this.expireDate;
  }

  public void setExpireDate(Date expireDate) {
    this.expireDate = expireDate;
  }
}
