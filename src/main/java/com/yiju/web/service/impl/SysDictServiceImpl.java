//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import com.yiju.web.dao.sys.SysDictDao;
import com.yiju.web.model.sys.SysDict;
import com.yiju.web.model.sys.SysDict.SysDictItem;
import com.yiju.web.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class SysDictServiceImpl implements SysDictService {
    @Autowired
    private SysDictDao sysDictDao;

    public SysDictServiceImpl() {
    }

    public Integer addDict(SysDict dict) {
        return this.sysDictDao.addDict(dict);
    }

    public boolean updateDict(SysDict dict) {
        this.sysDictDao.updateDict(dict);
        this.sysDictDao.removeItems(dict.getId());
        this.addDictItems(dict.getId(), dict.getItems());
        return true;
    }

    public boolean addDictItems(Integer dictId, List<SysDictItem> items) {
        return this.sysDictDao.addDictItems(dictId, items);
    }

    public boolean removeItems(Integer dictId) {
        return this.sysDictDao.removeItems(dictId);
    }

    public List<SysDict> getAllDicts() {
        return this.sysDictDao.getAllDicts();
    }

    public SysDict getDict(Integer id) {
        return this.sysDictDao.getDict(id);
    }

    public SysDict getDict(String alias) {
        return this.sysDictDao.getDictByAlias(alias);
    }

    public SysDictItem getDictItem(String dictAlias, String itemAlias) {
        SysDict dict = this.getDict(dictAlias);
        if (dict != null && dict.getItems() != null) {
            List<SysDictItem> items = dict.getItems();
            Iterator var5 = items.iterator();

            while (var5.hasNext()) {
                SysDictItem item = (SysDictItem) var5.next();
                if (item.getAlias() != null && item.getAlias().equals(itemAlias)) {
                    return item;
                }
            }
        }

        return null;
    }
}
