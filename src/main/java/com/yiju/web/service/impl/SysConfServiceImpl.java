//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import com.yiju.web.dao.sys.SysConfDao;
import com.yiju.web.model.sys.SysConf;
import com.yiju.web.service.SysConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysConfServiceImpl implements SysConfService {
    @Autowired
    private SysConfDao sysConfDao;

    public SysConfServiceImpl() {
    }

    public Integer addConf(SysConf conf) {
        return this.sysConfDao.addConf(conf);
    }

    public boolean removeConf(Integer id) {
        return this.sysConfDao.removeConf(id);
    }

    public boolean updateConf(SysConf conf) {
        if (conf.getId() == null || conf.getId().intValue() <= 0) {
            SysConf c = this.getConfByKey(conf.getKey());
            if (c == null) {
                this.sysConfDao.addConf(conf);
                return true;
            }

            conf.setId(c.getId());
        }

        return this.sysConfDao.updateConf(conf);
    }

    public List<SysConf> getAllConfs() {
        return this.sysConfDao.getAllConfs();
    }

    public List<SysConf> getAllConfs(String type) {
        List<SysConf> list = this.getAllConfs();
        List<SysConf> confs = new ArrayList();

        for (int i = 0; i < list.size(); ++i) {
            SysConf conf = (SysConf) list.get(i);
            if (conf.getType() == type || type != null && type.equals(conf.getType())) {
                confs.add(conf);
            }
        }

        return confs.size() > 0 ? confs : null;
    }

    public SysConf getConf(Integer id) {
        return this.sysConfDao.getConf(id);
    }

    public SysConf getConfByKey(String alias) {
        return this.sysConfDao.getConfByKey(alias);
    }
}
