//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import com.yiju.web.service.RedisService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import redis.clients.jedis.*;
import redis.clients.util.Pool;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {
    private static Logger logger = LoggerFactory.getLogger(RedisServiceImpl.class);
    private static Pool pool = null;
    private String hosts;
    private int maxTotal;
    private int maxIdle;
    private int maxWait;
    private int timeout;
    private int DEFAULT_LOCK_EXPIRE_TIME = 10;
    private String redisCli;

    public RedisServiceImpl() {
    }

    public String getHosts() {
        return this.hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public int getMaxTotal() {
        return this.maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxIdle() {
        return this.maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxWait() {
        return this.maxWait;
    }

    public void setMaxWait(int maxWait) {
        this.maxWait = maxWait;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getRedisCli() {
        return this.redisCli;
    }

    public void setRedisCli(String redisCli) {
        this.redisCli = redisCli;
    }

    @PostConstruct
    public void init() {
        logger.debug("init hosts:" + this.hosts);
        if (pool == null && this.hosts != null) {
            List<JedisShardInfo> redisHosts = this.getRedisHosts();
            if (redisHosts != null && redisHosts.size() > 0) {
                if (redisHosts.size() == 1) {
                    JedisShardInfo server = (JedisShardInfo) redisHosts.get(0);
                    pool = new JedisPool(this.getPoolConfig(), server.getHost(), server.getPort(), this.timeout, server.getPassword());
                } else {
                    pool = new ShardedJedisPool(this.getPoolConfig(), redisHosts);
                }
            }

        }
    }

    public List<JedisShardInfo> getRedisHosts() {
        List<JedisShardInfo> redisHosts = new ArrayList();
        String[] hostConfs = this.hosts.split(";");
        String[] var3 = hostConfs;
        int var4 = hostConfs.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            String hostConf = var3[var5];
            String host = hostConf.trim();
            if (!host.equals("")) {
                JedisShardInfo info = new JedisShardInfo(host);
                if (info.getPassword() != null && info.getPassword().trim().equals("")) {
                    info.setPassword((String) null);
                }

                redisHosts.add(info);
            }
        }

        return redisHosts;
    }

    public boolean setValue(String key, int value) {
        return this.setValue(key, String.valueOf(value));
    }

    public JedisPoolConfig getPoolConfig() {
        JedisPoolConfig redisConf = new JedisPoolConfig();
        logger.debug("redis config: maxIdle:" + this.maxIdle + ",maxWait:" + this.maxWait + ",maxTotal:" + this.maxTotal);
        redisConf.setMaxIdle(this.maxIdle);
        redisConf.setMaxWaitMillis((long) this.maxWait);
        redisConf.setMaxTotal(this.maxTotal);
        redisConf.setTestOnBorrow(false);
        redisConf.setTestOnReturn(true);
        return redisConf;
    }

    public boolean setValue(String key, int value, int seconds) {
        return this.setValue(key, String.valueOf(value), seconds);
    }

    public boolean setValue(String key, String value) {
        return this.setValue(key, value, 0);
    }

    private JedisCommands getResource() {
        JedisCommands jedis = (JedisCommands) pool.getResource();
        return jedis;
    }

    private void returnBrokenResource(JedisCommands jedis) {
        if (jedis != null) {
            pool.returnBrokenResource(jedis);
        }

    }

    private void returnResource(JedisCommands jedis) {
        if (jedis != null) {
            pool.returnResource(jedis);
        }

    }

    public boolean setValue(String key, String value, int seconds) {
        JedisCommands jedis = null;

        boolean var6;
        try {
            jedis = this.getResource();
            if (seconds > 0) {
                jedis.setex(key, seconds, value);
            } else {
                jedis.set(key, value);
            }

            return true;
        } catch (Exception var10) {
            var10.printStackTrace();
            this.returnBrokenResource(jedis);
            var6 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var6;
    }

    public String getValue(String key) {
        JedisCommands jedis = null;
        String value = null;

        Object var5;
        try {
            jedis = this.getResource();
            value = jedis.get(key);
            return value;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = null;
        } finally {
            this.returnResource(jedis);
        }

        return (String) var5;
    }

    public int getInt(String key) {
        int value = 0;
        String s = this.getValue(key);
        if (s != null) {
            try {
                value = Integer.parseInt(s);
            } catch (Exception var5) {
                var5.printStackTrace();
            }
        }

        return value;
    }

    public String lpop(String key) {
        JedisCommands jedis = null;
        String value = null;

        Object var5;
        try {
            jedis = this.getResource();
            value = jedis.lpop(key);
            return value;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = null;
        } finally {
            this.returnResource(jedis);
        }

        return (String) var5;
    }

    public String rpop(String key) {
        JedisCommands jedis = null;
        String value = null;

        Object var5;
        try {
            jedis = this.getResource();
            value = jedis.rpop(key);
            return value;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = null;
        } finally {
            this.returnResource(jedis);
        }

        return (String) var5;
    }

    public List<String> getList(String key) {
        JedisCommands jedis = null;
        List value = null;

        Object var5;
        try {
            jedis = this.getResource();
            value = jedis.lrange(key, 0L, -1L);
            return value;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = null;
        } finally {
            this.returnResource(jedis);
        }

        return (List) var5;
    }

    public boolean lpush(String key, String value) {
        JedisCommands jedis = null;

        boolean var5;
        try {
            jedis = this.getResource();
            jedis.lpush(key, new String[]{value});
            return true;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var5;
    }

    public boolean rpush(String key, String value) {
        JedisCommands jedis = null;

        boolean var5;
        try {
            jedis = this.getResource();
            jedis.rpush(key, new String[]{value});
            return true;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var5;
    }

    public boolean del(String key) {
        JedisCommands jedis = null;

        boolean var4;
        try {
            jedis = this.getResource();
            jedis.del(key);
            return true;
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
            var4 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var4;
    }

    public boolean incr(String key) {
        JedisCommands jedis = null;

        boolean var4;
        try {
            jedis = this.getResource();
            jedis.incr(key);
            return true;
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
            var4 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var4;
    }

    public boolean hdel(String key, String... field) {
        JedisCommands jedis = null;

        boolean var5;
        try {
            jedis = this.getResource();
            jedis.hdel(key, field);
            return true;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
            var5 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var5;
    }

    public boolean hincrby(String key, String field, long increment) {
        JedisCommands jedis = null;

        boolean var7;
        try {
            jedis = this.getResource();
            jedis.hincrBy(key, field, increment);
            return true;
        } catch (Exception var11) {
            var11.printStackTrace();
            this.returnBrokenResource(jedis);
            var7 = false;
        } finally {
            this.returnResource(jedis);
        }

        return var7;
    }

    public boolean hincr(String key, String field) {
        return this.hincrby(key, field, 1L);
    }

    public Map<String, String> hgetAll(String key) {
        JedisCommands jedis = null;

        HashMap var4;
        try {
            jedis = this.getResource();
            Map var3 = jedis.hgetAll(key);
            return var3;
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
            var4 = new HashMap();
        } finally {
            this.returnResource(jedis);
        }

        return var4;
    }

    public boolean sadd(String k, String... v) {
        JedisCommands jedis = null;
        boolean ret = false;

        try {
            jedis = this.getResource();
            jedis.sadd(k, v);
            ret = true;
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return ret;
    }

    public Long srem(String k, String... v) {
        Long ret = Long.valueOf(0L);
        JedisCommands jedis = null;

        try {
            jedis = this.getResource();
            ret = jedis.srem(k, v);
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return ret;
    }

    public Set<String> sdiff(String... k) {
        Jedis jedis = null;
        Set v = null;

        try {
            JedisCommands cmd = this.getResource();
            if (cmd instanceof Jedis) {
                jedis = (Jedis) cmd;
                v = jedis.sdiff(k);
            }
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public Long sdiffstore(String dk, String... k) {
        Jedis jedis = null;
        Long v = Long.valueOf(0L);

        try {
            JedisCommands cmd = this.getResource();
            if (cmd instanceof Jedis) {
                jedis = (Jedis) cmd;
                v = jedis.sdiffstore(dk, k);
            }
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public Long scard(String k) {
        JedisCommands jedis = null;
        Long v = null;

        try {
            jedis = this.getResource();
            v = jedis.scard(k);
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public List<String> srandmember(String k, Integer count) {
        JedisCommands jedis = null;
        List v = null;

        try {
            jedis = this.getResource();
            v = jedis.srandmember(k, count.intValue());
        } catch (Exception var9) {
            var9.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public String srandmember(String k) {
        JedisCommands jedis = null;
        String v = null;

        try {
            jedis = this.getResource();
            v = jedis.srandmember(k);
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public Set<String> smembers(String k) {
        JedisCommands jedis = null;
        Set v = null;

        try {
            jedis = this.getResource();
            v = jedis.smembers(k);
        } catch (Exception var8) {
            var8.printStackTrace();
            this.returnBrokenResource(jedis);
        } finally {
            this.returnResource(jedis);
        }

        return v;
    }

    public boolean setnx(String k, String v, int expiresInSeconds) {
        JedisCommands jedis = null;

        boolean var7;
        try {
            jedis = this.getResource();
            long ret = jedis.setnx(k, v).longValue();
            if (ret != 1L) {
                return false;
            }

            jedis.expire(k, expiresInSeconds);
            var7 = true;
        } catch (Exception var11) {
            var11.printStackTrace();
            this.returnBrokenResource(jedis);
            return false;
        } finally {
            this.returnResource(jedis);
        }

        return var7;
    }

    public Pool getPool() {
        return pool;
    }

    public boolean tryLock(String lockName, int timeoutInSeconds, int expiresInSeconds) {
        long nano = System.nanoTime();
        JedisCommands jedis = null;

        try {
            jedis = this.getResource();

            do {
                long ret = jedis.setnx(lockName, "1").longValue();
                if (ret == 1L) {
                    jedis.expire(lockName, expiresInSeconds);
                    boolean var9 = true;
                    return var9;
                }

                Thread.sleep(500L);
            } while (System.nanoTime() - nano < TimeUnit.SECONDS.toNanos((long) timeoutInSeconds));

            boolean var15 = false;
            return var15;
        } catch (InterruptedException var13) {
            var13.printStackTrace();
        } finally {
            this.returnResource(jedis);
        }

        return false;
    }

    public boolean tryLock(String lockName, int timeoutInSeconds) {
        return this.tryLock(lockName, timeoutInSeconds, this.DEFAULT_LOCK_EXPIRE_TIME);
    }

    public boolean unLock(String lockName) {
        JedisCommands jedis = null;

        try {
            jedis = this.getResource();
            jedis.del(lockName);
            boolean var3 = true;
            return var3;
        } catch (Exception var7) {
            var7.printStackTrace();
        } finally {
            this.returnResource(jedis);
        }

        return false;
    }

    private String execInShell(String cmd) {
        String[] cmds = new String[]{"/bin/sh", "-c", cmd};
        Process process = null;
        StringBuffer ret = new StringBuffer();

        try {
            process = Runtime.getRuntime().exec(cmds);
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";

            while ((line = input.readLine()) != null) {
                logger.info("SHELL RETURN:" + line);
                ret.append(line).append("\n");
            }

            input.close();
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        return ret.toString();
    }

    public boolean execInRedisCli(String cmd, String redisParam) {
        List<JedisShardInfo> redisHosts = this.getRedisHosts();
        JedisShardInfo jedisShardInfo = (JedisShardInfo) redisHosts.get((new Random()).nextInt(redisHosts.size()));
        String host = jedisShardInfo.getHost();
        int port = jedisShardInfo.getPort();
        String pwd = jedisShardInfo.getPassword();
        StringBuffer cmdSb = new StringBuffer();
        cmdSb.append(cmd + " | ");
        cmdSb.append(this.redisCli == null ? "redis-cli" : this.redisCli);
        cmdSb.append(" -h ").append(host);
        if (port > 0) {
            cmdSb.append(" -p ").append(port);
        }

        if (!StringUtils.isEmpty(pwd)) {
            cmdSb.append(" -a ").append(pwd);
        }

        if (redisParam != null) {
            cmdSb.append(" ").append(redisParam);
        }

        this.execInShell(cmdSb.toString());
        return false;
    }

    public boolean execInRedisCli(String cmd) {
        return this.execInRedisCli(cmd, (String) null);
    }

    public static void main(String[] args) {
        RedisServiceImpl redisImpl = new RedisServiceImpl();
        redisImpl.setHosts("redis://:cc_redis2017@192.168.3.100:26001;");
        redisImpl.setMaxIdle(200);
        redisImpl.setMaxWait(5000);
        redisImpl.setMaxTotal(500);
        redisImpl.setTimeout(5000);
        redisImpl.init();
        System.out.println(redisImpl.getResource().getClass());
        redisImpl.execInRedisCli("cat /Users/baboy/tmp/cc/redis/2017-09-01/mobile_sample_010", (String) null);
    }
}
