//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service.impl;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.yiju.web.service.QRCodeService;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

@Service
public class QRCodeServiceImpl implements QRCodeService {
    public QRCodeServiceImpl() {
    }

    public BufferedImage createQRCode(String s, int width, int height) {
        Map<EncodeHintType, Object> hintMap = new EnumMap(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hintMap.put(EncodeHintType.MARGIN, Integer.valueOf(1));
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        try {
            BitMatrix matrix = (new MultiFormatWriter()).encode(s, BarcodeFormat.QR_CODE, width, height, hintMap);
            BufferedImage image = new BufferedImage(matrix.getWidth(), matrix.getHeight(), 1);
            image.createGraphics();
            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < matrix.getWidth(); ++i) {
                for (int j = 0; j < matrix.getWidth(); ++j) {
                    if (matrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }

            return image;
        } catch (Exception var10) {
            var10.printStackTrace();
            return null;
        }
    }

    public boolean saveImage(BufferedImage image, String fp) {
        try {
            File f = new File(fp);
            String ext = null;
            String fn = f.getName();
            if (fn.indexOf(".") >= 0) {
                ext = fn.substring(fn.lastIndexOf(".") + 1);
            }

            File dir = f.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }

            return ImageIO.write(image, ext, f);
        } catch (IOException var7) {
            var7.printStackTrace();
            return false;
        }
    }

    public boolean createQRCode(String s, int width, int height, String fp) {
        BufferedImage image = this.createQRCode(s, width, height);
        return this.saveImage(image, fp);
    }

    public byte[] getBytes(BufferedImage image) {
        byte[] bytes = null;

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            boolean flag = ImageIO.write(image, "gif", out);
            if (flag) {
                bytes = out.toByteArray();
            }

            out.close();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        return bytes;
    }

    public void parseQRCode() {
    }
}
