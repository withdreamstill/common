//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.service;

import com.yiju.web.model.sys.SysDict;
import com.yiju.web.model.sys.SysDict.SysDictItem;

import java.util.List;

public interface SysDictService {
    Integer addDict(SysDict var1);

    boolean updateDict(SysDict var1);

    boolean addDictItems(Integer var1, List<SysDictItem> var2);

    boolean removeItems(Integer var1);

    List<SysDict> getAllDicts();

    SysDict getDict(Integer var1);

    SysDict getDict(String var1);

    SysDictItem getDictItem(String var1, String var2);
}
