//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ActionEventBroadcaster implements ApplicationListener, ApplicationContextAware {
    private Logger logger = LoggerFactory.getLogger(ActionEventBroadcaster.class);
    private static Map<String, List> listeners = new HashMap();
    private static String LISTEN_HANDLE_METHOD_NAME = "onEventReceived";
    private static ApplicationContext ctx;

    public ActionEventBroadcaster() {
    }

    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ActionEvent) {
            ActionEvent action = (ActionEvent) event;
            synchronized (event) {
                if (action.isDone()) {
                    return;
                }

                action.setDone(true);
            }

            Object source = event.getSource();
            String id = action.getId();
            List a = (List) listeners.get(id);
            if (a == null) {
                return;
            }

            Object data = ((ActionEvent) event).getData();

            for (int i = 0; i < a.size(); ++i) {
                Object o = a.get(i);

                try {
                    o = ctx.getBean(o.getClass());
                } catch (Exception var13) {
                    var13.printStackTrace();
                }

                try {
                    Class c = o.getClass();
                    Method m = c.getDeclaredMethod(LISTEN_HANDLE_METHOD_NAME, new Class[]{Object.class, String.class, Object.class});
                    m.invoke(o, new Object[]{source, id, data});
                } catch (Exception var12) {
                    var12.printStackTrace();
                }
            }
        }

    }

    public static synchronized void addListener(String id, Object obj) {
        List a = (List) listeners.get(id);
        if (a == null) {
            a = new ArrayList();
        }

        ((List) a).add(obj);
        listeners.put(id, a);
    }

    public static synchronized void removeListener(String id, Object obj) {
        List a = (List) listeners.get(id);
        if (a != null) {
            for (int i = 0; i < a.size(); ++i) {
                if (a.get(i) == obj) {
                    a.remove(obj);
                    listeners.put(id, a);
                    return;
                }
            }
        }

    }

    public static void publishEvent(Object source, String id, Object data) {
        ActionEvent event = new ActionEvent(source, id, data);
        ctx.publishEvent(event);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }
}
