//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.event;

import org.springframework.core.task.TaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class AsyncAppEventMulticaster implements TaskExecutor {
    ExecutorService service;
    AsyncAppEventMulticaster multicaster;

    AsyncAppEventMulticaster(AsyncAppEventMulticaster this$0) {
        this.multicaster = this$0;
        this.service = Executors.newCachedThreadPool();
    }

    public void execute(Runnable task) {
        this.service.execute(task);
    }
}
