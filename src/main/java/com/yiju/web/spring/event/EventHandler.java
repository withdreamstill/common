//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.event;

public interface EventHandler {
    void onEventReceived(Object var1, String var2, Object var3);
}
