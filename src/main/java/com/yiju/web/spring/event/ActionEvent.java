//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.event;

import org.springframework.context.ApplicationEvent;

public class ActionEvent extends ApplicationEvent {
    private String id;
    private Object data;
    private boolean done = false;

    public ActionEvent(Object source) {
        super(source);
    }

    public ActionEvent(String id, Object data) {
        super(id);
        this.id = id;
        this.data = data;
    }

    public ActionEvent(Object source, String id, Object data) {
        super(source);
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isDone() {
        return this.done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
