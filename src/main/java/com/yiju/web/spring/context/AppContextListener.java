//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.context;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

public class AppContextListener extends ContextLoaderListener {
    private static ServletContext servletContext;

    public AppContextListener() {
    }

    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        servletContext = event.getServletContext();
    }

    public static ApplicationContext getRootContext() {
        ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        return ctx;
    }
}
