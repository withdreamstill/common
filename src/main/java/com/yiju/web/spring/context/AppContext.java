//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.spring.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;

@Component
public class AppContext extends ApplicationObjectSupport {
    private static ApplicationContext ctx;
    private static ApplicationContext dispatcher;
    private static ServletContext servletContext;
    private static AppContext instance;

    public AppContext() {
        instance = this;
    }

    public static ApplicationContext getRootContext() {
        return AppContextListener.getRootContext();
    }

    public static ApplicationContext getContext() {
        return instance.getApplicationContext();
    }
}
