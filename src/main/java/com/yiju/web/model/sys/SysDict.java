//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.model.sys;

import com.yiju.web.model.Model;

import java.util.Date;
import java.util.List;

public class SysDict extends Model {
    private static final long serialVersionUID = -7095108879458789231L;
    private Integer id;
    private String title;
    private String type;
    private String alias;
    private String note;
    private Integer createUid;
    private Date createTime;
    private Integer updateUid;
    private Date updateTime;
    private List<SysDict.SysDictItem> items;

    public SysDict() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCreateUid() {
        return this.createUid;
    }

    public void setCreateUid(Integer createUid) {
        this.createUid = createUid;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateUid() {
        return this.updateUid;
    }

    public void setUpdateUid(Integer updateUid) {
        this.updateUid = updateUid;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<SysDict.SysDictItem> getItems() {
        return this.items;
    }

    public void setItems(List<SysDict.SysDictItem> items) {
        this.items = items;
    }

    public static class SysDictItem extends Model {
        private static final long serialVersionUID = 4208300314227572060L;
        private Integer id;
        private Integer dictId;
        private Integer value;
        private String title;
        private String alias;
        private String note;
        private Integer sortNo;
        private String createUid;
        private Date createTime;
        private Date updateUid;
        private Date updateTime;
        private Object metadata;

        public SysDictItem() {
        }

        public Integer getId() {
            return this.id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getDictId() {
            return this.dictId;
        }

        public void setDictId(Integer dictId) {
            this.dictId = dictId;
        }

        public Integer getValue() {
            return this.value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAlias() {
            return this.alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getNote() {
            return this.note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Integer getSortNo() {
            return this.sortNo;
        }

        public void setSortNo(Integer sortNo) {
            this.sortNo = sortNo;
        }

        public String getCreateUid() {
            return this.createUid;
        }

        public void setCreateUid(String createUid) {
            this.createUid = createUid;
        }

        public Date getCreateTime() {
            return this.createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public Date getUpdateUid() {
            return this.updateUid;
        }

        public void setUpdateUid(Date updateUid) {
            this.updateUid = updateUid;
        }

        public Date getUpdateTime() {
            return this.updateTime;
        }

        public void setUpdateTime(Date updateTime) {
            this.updateTime = updateTime;
        }

        public Object getMetadata() {
            return this.metadata;
        }

        public void setMetadata(Object metadata) {
            this.metadata = metadata;
        }
    }
}
