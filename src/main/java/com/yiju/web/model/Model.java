//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.model;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Model implements Serializable {
    private static final long serialVersionUID = 78464017555399406L;

    public Model() {
    }

    public Object deepClone() {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            oo.writeObject(this);
            ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
            ObjectInputStream oi = new ObjectInputStream(bi);
            Object copy = oi.readObject();
            oi.close();
            bi.close();
            oo.close();
            bo.close();
            return copy;
        } catch (Exception var6) {
            var6.printStackTrace();
            return null;
        }
    }

    public String toString() {
        GsonBuilder gb = new GsonBuilder();
        Gson g = gb.create();
        return g.toJson(this);
    }

    public Map<String, Object> toMap() {
        return this.toMapIgnoreFields(new String[0]);
    }

    public Map<String, Object> toMapIgnoreFields(String... ignoreFields) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(Feature.ESCAPE_NON_ASCII, true);
        mapper.setSerializationInclusion(Include.NON_NULL);
        Map map = (Map) mapper.convertValue(this, HashMap.class);
        if (ignoreFields != null && ignoreFields.length > 0) {
            String[] var4 = ignoreFields;
            int var5 = ignoreFields.length;

            for (int var6 = 0; var6 < var5; ++var6) {
                String k = var4[var6];
                map.remove(k);
            }
        }

        return map;
    }
}
