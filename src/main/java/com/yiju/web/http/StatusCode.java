//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.http;

public class StatusCode {
    public static String STATUS_CODE_KEY = "record";
    public static String STATUS_MSG_KEY = "msg";
    public static String STATUS_DATA_KEY = "data";
    public static int FAILURE = -10;
    public static int SUCCESS = 0;
    public static int ERROR = -20;
    public static int ERROR_PARAM = -30;
    public static int ERROR_PARAM_SERVER = -31;
    public static int ERROR_AUTH = -40;
    public static int ERROR_AUTH_OTHERS = -50;
    public static int ERROR_ACL = -60;

    public StatusCode() {
    }
}
