//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yiju.web.utils.StringUtils;
import com.yiju.web.utils.URLUTF8Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpRequest {
    private Logger logger = LoggerFactory.getLogger(HttpRequest.class);
    public static String DATA_TYPE_JSON = "json";
    public static String DATA_TYPE_ENVELOPE_JSON = "envelope_json";
    public static String DATA_TYPE_STRING = "string";
    private String dataType;
    private String charset;
    private Map<String, String> headers;
    private byte[] postData;

    public HttpRequest() {
        this.dataType = DATA_TYPE_STRING;
        this.charset = "UTF-8";
    }

    public String getDataType() {
        return this.dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Map getHeaders() {
        return this.headers;
    }

    public void setHeaders(Map headers) {
        this.headers = headers;
    }

    public void addHeader(String k, String v) {
        if (this.headers == null) {
            this.headers = new HashMap();
        }

        this.headers.put(k, v);
    }

    public byte[] getPostData() {
        return this.postData;
    }

    public void setPostData(byte[] postData) {
        this.postData = postData;
    }

    public HttpResponse query(String url) throws Exception {
        return this.query(url, (Map) null, HttpMethod.GET);
    }

    public HttpResponse query(String url, Map<String, String> params, HttpMethod method) throws Exception {
        HttpResponse response = new HttpResponse();
        String connUrl = url;
        method = method == null ? HttpMethod.GET : method;
        if (method == HttpMethod.GET) {
            connUrl = getUrl(url, params);
        }

        String decodeUrl = encodeUrl(connUrl);
        byte[] postBody = this.postData;
        InputStream is = null;
        HttpURLConnection conn = null;

        HttpResponse var38;
        try {
            conn = (HttpURLConnection) (new URL(decodeUrl)).openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            String sep;
            if (this.headers != null) {
                Iterator var10 = this.headers.keySet().iterator();

                while (var10.hasNext()) {
                    String k = (String) var10.next();
                    String v = (String) this.headers.get(k);
                    conn.setRequestProperty(k, v);
                    if (k.equalsIgnoreCase("content-type") && "application/json".equalsIgnoreCase(v) && method == HttpMethod.POST && params != null) {
                        sep = (new ObjectMapper()).writeValueAsString(params);
                        postBody = sep.getBytes(this.charset);
                    }
                }
            }

            switch (method.ordinal()) {
                case 1:
                    conn.setRequestMethod("GET");
                    conn.setUseCaches(true);
                    break;
                case 2:
                    conn.setUseCaches(false);
                    conn.setRequestMethod("POST");
                    if (postBody != null || params != null) {
                        conn.setDoOutput(true);
                        OutputStream outputStream = conn.getOutputStream();
                        if (postBody != null) {
                            outputStream.write(postBody);
                        } else if (params != null) {
                            StringBuffer sb = new StringBuffer();
                            Iterator<String> iterator = params.keySet().iterator();

                            String k;
                            for (sep = null; iterator.hasNext(); sep = "&") {
                                k = (String) iterator.next();
                                String v = (String) params.get(k);
                                if (sep != null) {
                                    sb.append(sep);
                                }

                                if (v == null) {
                                    v = "";
                                }

                                sb.append(k + "=" + URLEncoder.encode(v, "UTF-8"));
                            }

                            k = sb.toString();
                            outputStream.write(k.getBytes("UTF-8"));
                        }

                        outputStream.flush();
                        outputStream.close();
                    }
            }

            is = conn.getInputStream();
            int contentLength = conn.getContentLength();
            int PER_BUFFER_SIZE = 102400;
            if (contentLength <= 0) {
                contentLength = PER_BUFFER_SIZE;
            }

            byte[] data = null;
            if (this.dataType.equals(DATA_TYPE_STRING) || this.dataType.equals(DATA_TYPE_JSON) || this.dataType == DATA_TYPE_ENVELOPE_JSON) {
                data = new byte[contentLength];
            }

            byte[] buffer = new byte[81920];
            int len;
            int dataLength;
            for (dataLength = 0; (len = is.read(buffer)) >= 0; dataLength += len) {
                if (dataLength + len > contentLength) {
                    contentLength += PER_BUFFER_SIZE > len ? PER_BUFFER_SIZE : len;
                    byte[] tmp = new byte[contentLength];
                    System.arraycopy(data, 0, tmp, 0, dataLength);
                    data = tmp;
                }

                if (data != null) {
                    System.arraycopy(buffer, 0, data, dataLength, len);
                }
            }

            if (data != null) {
                String s = new String(data, 0, dataLength, this.charset);
                if (this.dataType.equals(DATA_TYPE_ENVELOPE_JSON)) {
                    Map ret = (Map) StringUtils.getJson(s);
                    response.setStatus(Integer.parseInt(ret.get("status").toString()));
                    response.setData(ret.get("data"));
                } else if (this.dataType.equals(DATA_TYPE_JSON)) {
                    response.setData(StringUtils.getJson(s));
                } else {
                    response.setData(s);
                }
            }

            var38 = response;
        } catch (Exception var25) {
            response.setStatus(StatusCode.ERROR);
            response.setMsg(var25.getMessage());
            this.logger.debug("HTTP REQUEST:" + decodeUrl);
            throw var25;
        } finally {
            try {
                is.close();
            } catch (Exception var24) {
                var24.printStackTrace();
            }

        }

        return var38;
    }

    public static String getUrl(String url, Map<String, String> param, boolean append) {
        try {
            String k;
            if (param != null) {
                String re = "\\{([^\\{\\}]+)\\}";
                Pattern p = Pattern.compile(re);
                Matcher m = p.matcher(url);

                while (m.find()) {
                    String k2 = m.group(1) + "";
                    if (param.containsKey(k2)) {
                        k = "{" + k2 + "}";
                        Object v = param.get(k2);
                        v = v == null ? "" : String.valueOf(v);
                        url = url.replace(k, URLUTF8Encoder.encode((String) v));
                        param.remove(k2);
                    }
                }
            }

            Map<String, String> map = new HashMap();
            URL aURL = new URL(url);
            String q = aURL.getQuery();
            if (q != null) {
                String[] arr = q.split("&");

                for (int i = 0; i < arr.length; ++i) {
                    String[] a = arr[i].split("=");
                    k = a[0];
                    String v = a.length > 1 ? a[1] : "";
                    k = "\\{([^\\{\\}]+)\\}";
                    Pattern p = Pattern.compile(k);
                    Matcher m = p.matcher(v);
                    if (m.find()) {
                        String k2 = m.group(1);
                        v = "";
                        if (param != null && param.containsKey(k2)) {
                            v = (String) param.get(k2);
                            param.remove(k2);
                        }
                    }

                    map.put(k, v);
                }
            }

            if (append && param != null) {
                Iterator ite = param.keySet().iterator();

                while (ite.hasNext()) {
                    k = (String) ite.next();
                    String v = (String) param.get(k);
                    map.put(k, v);
                }
            }

            int pos = url.indexOf(63);
            k = pos > 0 ? url.substring(0, pos) : url;
            StringBuffer newUrl = new StringBuffer(k);
            k = null;
            Iterator var26 = map.keySet().iterator();

            while (var26.hasNext()) {
                k = (String) var26.next();
                k = k == null ? "?" : "&";
                newUrl.append(k);
                newUrl.append(k + "=" + URLUTF8Encoder.encode((String) map.get(k)));
            }

            return newUrl.toString();
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        }
    }

    public static String getUrl(String url, Map<String, String> param) {
        return getUrl(url, param, true);
    }

    public static String encodeUrl(String url) {
        StringBuffer newUrl = null;

        try {
            URL aURL = new URL(url);
            newUrl = new StringBuffer(aURL.getProtocol());
            newUrl.append("://");
            newUrl.append(aURL.getHost());
            if (aURL.getPort() > 0) {
                newUrl.append(":").append(aURL.getPort());
            }

            String[] arr = aURL.getPath().split("/");
            int n = arr.length;

            for (int i = 0; i < n; ++i) {
                String s = arr[i];
                if (!s.equals("")) {
                    newUrl.append("/").append(URLEncoder.encode(s, "UTF-8"));
                }
            }

            if (aURL.getQuery() != null) {
                newUrl.append("?");
                String[] a = aURL.getQuery().split("&");
                int n1 = a.length;
                String sep = null;

                for (int i = 0; i < n1; ++i) {
                    String s = a[i];
                    String[] a2 = s.split("=");
                    int n2 = a2.length;
                    if (n2 >= 1) {
                        String k = a2[0];
                        String v = n2 > 1 ? a2[1] : null;
                        sep = sep == null ? "" : "&";
                        newUrl.append(sep).append(k);
                        if (v != null) {
                            newUrl.append("=").append(URLEncoder.encode(v, "UTF-8"));
                        }
                    }
                }
            }
        } catch (Exception var14) {
            var14.printStackTrace();
        }

        return newUrl.toString();
    }

    public static void main(String[] args) throws Exception {
        Map p = new HashMap();
        p.put("cityCode", (Object) null);
        p.put("mobile", "15801207618");
        p.put("name", "其实还好好");
        p.put("channel", (Object) null);
        p.put("idNo", (Object) null);
        p.put("channelTag", (Object) null);
        p.put("marketType", (Object) null);
        p.put("loanAmount", "340000");
        HttpRequest request1 = new HttpRequest();
        request1.setDataType(DATA_TYPE_ENVELOPE_JSON);

        try {
            Object r = request1.query("http://127.0.0.1:9004/api/v1/customer/apply", p, HttpMethod.POST);
            System.out.println(r);
        } catch (Exception var4) {
            ;
        }

    }
}
