//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.http;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.yiju.web.model.Model;

public class HttpResponse<T> extends Model {
    private static final long serialVersionUID = 3027878830870620291L;
    private int status;
    private String msg;
    private Object data;

    public HttpResponse() {
        this.status = StatusCode.SUCCESS;
    }

    public HttpResponse(String jsonString) {
        this.status = StatusCode.SUCCESS;

        try {
            GsonBuilder gb = new GsonBuilder();
            Gson g = gb.create();
            T data = g.fromJson(jsonString, (new TypeToken<T>() {
            }).getType());
            if (data != null) {
                this.data = data;
            }
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    public HttpResponse(int status, String msg) {
        this.status = StatusCode.SUCCESS;
        this.status = status;
        this.msg = msg;
    }

    public HttpResponse(int status, String msg, Object data) {
        this.status = StatusCode.SUCCESS;
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public int getStatus() {
        return this.status;
    }

    public HttpResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMsg() {
        return this.msg;
    }

    public HttpResponse setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getData() {
        return this.data;
    }

    public HttpResponse setData(Object data) {
        this.data = data;
        return this;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return this.status == StatusCode.SUCCESS;
    }

    @JsonIgnore
    public String getErrorMessage() {
        if (this.isSuccess()) {
            return null;
        } else {
            if (this.msg == null) {
                this.msg = "Server Error";
            }

            return this.msg;
        }
    }

    public static HttpResponse<?> getErrorParamResponse() {
        HttpResponse<?> response = new HttpResponse();
        response.setStatus(StatusCode.ERROR_PARAM);
        response.setMsg("参数错误");
        return response;
    }

    public static HttpResponse<?> getSuccessResponse() {
        HttpResponse<?> response = new HttpResponse();
        response.setStatus(StatusCode.SUCCESS);
        response.setMsg("请求成功");
        return response;
    }

    public static HttpResponse<?> getFailureResponse() {
        HttpResponse<?> response = new HttpResponse();
        response.setStatus(StatusCode.FAILURE);
        response.setMsg("操作失败");
        return response;
    }
}
