//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.filter;

import org.apache.commons.io.IOUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class HttpRequestCacheServlet extends HttpServletRequestWrapper {
    private HttpServletRequest request;
    private ByteArrayOutputStream cachedOutputStream;

    public HttpRequestCacheServlet(HttpServletRequest request) {
        super(request);
        this.request = request;
    }

    public synchronized ServletInputStream getInputStream() throws IOException {
        if (this.cachedOutputStream == null) {
            this.cachedOutputStream = new ByteArrayOutputStream();
            IOUtils.copy(super.getInputStream(), this.cachedOutputStream);
        }

        ServletInputStream inputStream = new HttpRequestCacheServlet.ContentCachedInputStream(this.cachedOutputStream);
        if (inputStream.markSupported()) {
            inputStream.reset();
        }

        return inputStream;
    }

    private class ContentCachedInputStream extends ServletInputStream {
        private ByteArrayInputStream is;
        private boolean isFinished = false;

        public ContentCachedInputStream(ByteArrayOutputStream os) {
            this.is = new ByteArrayInputStream(os.toByteArray());
        }

        public int read() throws IOException {
            int c = this.is.read();
            if (c == -1) {
                this.isFinished = true;
            }

            return c;
        }

        public boolean isFinished() {
            return this.isFinished;
        }

        public boolean isReady() {
            return true;
        }

        public void setReadListener(ReadListener readListener) {
        }

        public void close() throws IOException {
            this.is.close();
        }
    }
}
