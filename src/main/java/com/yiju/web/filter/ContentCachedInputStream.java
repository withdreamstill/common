//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.filter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

class ContentCachedInputStream extends ServletInputStream {
  private HttpRequestCacheServlet var1;
  private ByteArrayInputStream is;
  private boolean isFinished;

  public ContentCachedInputStream(HttpRequestCacheServlet var1, ByteArrayOutputStream os) {
    this.var1 = var1;
    this.isFinished = false;
    this.is = new ByteArrayInputStream(os.toByteArray());
  }

  public int read() throws IOException {
    int c = this.is.read();
    if(c == -1) {
      this.isFinished = true;
    }

    return c;
  }

  public boolean isFinished() {
    return this.isFinished;
  }

  public boolean isReady() {
    return true;
  }

  public void setReadListener(ReadListener readListener) {
  }

  public void close() throws IOException {
    this.is.close();
  }
}
