//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class HttpRequestCacheFilter implements Filter {
    public HttpRequestCacheFilter() {
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpRequestCacheServlet cacheServlet = new HttpRequestCacheServlet((HttpServletRequest) request);
            chain.doFilter(cacheServlet, response);
        } else {
            chain.doFilter(request, response);
        }

    }

    public void destroy() {
    }
}
