//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.auth.dao;

import com.yiju.web.auth.model.UserAuthToken;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAuthDao {
    int addAuthToken(UserAuthToken var1);

    boolean updateAuthToken(UserAuthToken var1);

    List<UserAuthToken> getAuthTokens(@Param("uid") int var1, @Param("type") String var2, @Param("domain") String var3, @Param("concurLoginNum") int var4);

    UserAuthToken getAuthTokenByUkey(@Param("uid") int var1, @Param("ukey") String var2);
}
