//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.auth.interceptor;

import com.yiju.web.acl.annotation.Auth;
import com.yiju.web.auth.model.UserAuthToken;
import com.yiju.web.auth.service.UserAuthService;
import com.yiju.web.auth.utils.SessionKey;
import com.yiju.web.validation.exception.AclException;
import com.yiju.web.validation.exception.AuthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserAuthService userAuthService;

    public AuthInterceptor() {
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return this.authCheck(request, response, handler, true);
    }

    public boolean authCheck(HttpServletRequest request, HttpServletResponse response, Object handler, boolean checkCookie) throws Exception {
        HandlerMethod method = null;
        if (handler instanceof HandlerMethod) {
            method = (HandlerMethod) handler;
        }

        if (method == null) {
            throw new AclException("auth.error", new Object[0]);
        } else {
            Auth auth = (Auth) method.getMethodAnnotation(Auth.class);
            if (auth != null && !auth.auth()) {
                return true;
            } else {
                try {
                    UserAuthToken userAuthToken = null;
                    if (checkCookie) {
                        userAuthToken = this.userAuthService.getCheckedCookieAuthToken(request);
                        this.logger.debug("=====AUTH=====" + userAuthToken);
                    } else {
                        userAuthToken = this.userAuthService.getCheckedAuthToken(request);
                    }

                    if (userAuthToken == null) {
                        throw new AuthException("auth.error", new Object[0]);
                    } else {
                        request.setAttribute(SessionKey.AUTH_TOKEN, userAuthToken);
                        request.setAttribute(SessionKey.UID, userAuthToken.getUid());
                        return super.preHandle(request, response, handler);
                    }
                } catch (Exception var8) {
                    var8.printStackTrace();
                    throw new AuthException("auth.error", new Object[0]);
                }
            }
        }
    }
}
