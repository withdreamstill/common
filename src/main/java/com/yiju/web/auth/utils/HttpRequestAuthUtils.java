//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.auth.utils;

import com.yiju.web.utils.Utils;
import com.yiju.web.utils.encrypt.YiJuEncrypt;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

public class HttpRequestAuthUtils {
    private static int tokenExpiresInSeconds = 600;

    public HttpRequestAuthUtils() {
    }

    public static boolean checkRequest(HttpServletRequest request, int expiresInSeconds, String checkContent) {
        String token = request.getHeader(HttpHeaderField.AUTH_REQUEST_SIGNATURE);
        if (token == null) {
            return false;
        } else {
            Map map = YiJuEncrypt.parse(token);
            if (map == null) {
                return false;
            } else {
                String ver = (String) map.get("ver");
                if (!"AB".equalsIgnoreCase(ver)) {
                    return false;
                } else {
                    int t = Utils.parseInt(map.get("time")).intValue();
                    long timeDiff = Math.abs((new Date()).getTime() - (long) t * 1000L);
                    if (timeDiff > (long) (expiresInSeconds * 1000)) {
                        return false;
                    } else {
                        if (checkContent != null) {
                            String salt = (String) map.get("salt");
                            String s = DigestUtils.sha1Hex(checkContent);
                            String content = (String) map.get("content");
                            if (!s.equalsIgnoreCase(content)) {
                                return false;
                            }
                        }

                        return true;
                    }
                }
            }
        }
    }

    public static boolean checkRequest(HttpServletRequest request, int expiresInSeconds) {
        return checkRequest(request, tokenExpiresInSeconds, (String) null);
    }

    public static boolean checkRequest(HttpServletRequest request) {
        return checkRequest(request, tokenExpiresInSeconds, (String) null);
    }
}
