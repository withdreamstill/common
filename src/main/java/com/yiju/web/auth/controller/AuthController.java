//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.auth.controller;

import com.yiju.web.acl.annotation.Auth;
import com.yiju.web.auth.model.UserAuthToken;
import com.yiju.web.auth.service.UserAuthService;
import com.yiju.web.utils.NetworkUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {
    @Autowired
    private UserAuthService userAuthService;

    public AuthController() {
    }

    @RequestMapping({"/api/v1/auth/createUkey"})
    @ResponseBody
    @Auth(
            auth = false,
            acl = false
    )
    public Object create(Integer uid, HttpServletRequest req) {
        String uAgent = req.getHeader("user-agent");
        String ip = NetworkUtils.getIpAddress(req);
        UserAuthToken token = this.userAuthService.createUserAuthToken(uid.intValue(), (String) null, (String) null, (String) null, uAgent, ip);
        return token;
    }

    @ResponseBody
    @RequestMapping({"/api/v1/auth/parseUkey"})
    @Auth(
            auth = false,
            acl = false
    )
    public Object parse(String ukey, HttpServletRequest req) {
        String uAgent = req.getHeader("user-agent");
        String ip = NetworkUtils.getIpAddress(req);
        this.userAuthService.parseUkey(ukey);
        UserAuthToken token = this.userAuthService.getCheckedAuthToken(ukey, req);
        return token;
    }
}
