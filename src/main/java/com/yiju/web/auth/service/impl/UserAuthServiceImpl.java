package com.yiju.web.auth.service.impl;

import com.yiju.web.auth.dao.UserAuthDao;
import com.yiju.web.auth.model.UserAuthToken;
import com.yiju.web.auth.service.UserAuthService;
import com.yiju.web.auth.utils.Const;
import com.yiju.web.auth.utils.CookieKey;
import com.yiju.web.utils.CookieUtils;
import com.yiju.web.utils.NetworkUtils;
import com.yiju.web.utils.StringUtils;
import com.yiju.web.utils.Utils;
import com.yiju.web.utils.encrypt.AesUtils;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserAuthServiceImpl
        implements UserAuthService
{
    private Logger logger = LoggerFactory.getLogger(getClass());
    private static String SEGMENT_LEN_FORMAT = "000";
    private static int SALT_BITS = 8;
    private static int CHECK_CODE_BITS = 4;
    private static String UKEY_PREFIX = "AA";
    @Autowired
    private UserAuthDao userAuthDao;
    @Value("${auth.cookie.expire}")
    private int expireTimeInSeconds;
    @Value("${auth.concur.num}")
    private int concurLoginNum;
    @Value("${auth.concur}")
    private String authConcurConfString;
    private Map<String, Integer> authConcurConf = new HashMap();
    @Value("${auth.cookie.domain}")
    private String mainDomain;

    public int addAuthToken(UserAuthToken token)
    {
        return this.userAuthDao.addAuthToken(token);
    }

    public boolean updateAuthToken(UserAuthToken token)
    {
        return this.userAuthDao.updateAuthToken(token);
    }

    @PostConstruct
    public void init()
    {
        if (this.authConcurConfString != null)
        {
            String[] a = this.authConcurConfString.split(";");
            for (int i = 0; i < a.length; i++)
            {
                String[] a2 = a[i].split(":");
                if (a2.length == 2)
                {
                    Integer v = Utils.parseInt(a2[1]);
                    if (v != null) {
                        this.authConcurConf.put(a2[0], v);
                    }
                }
            }
        }
    }

    public List<UserAuthToken> getAuthTokens(int uid)
    {
        return getAuthTokens(uid, null);
    }

    public List<UserAuthToken> getAuthTokens(int uid, String platform)
    {
        int num = this.concurLoginNum;
        if ((platform != null) && (this.authConcurConf.containsKey(platform))) {
            num = ((Integer)this.authConcurConf.get(platform)).intValue();
        }
        return this.userAuthDao.getAuthTokens(uid, platform, null, num);
    }

    public List<UserAuthToken> getAuthTokens(int uid, String domain, String platform)
    {
        int num = this.concurLoginNum;
        if ((platform != null) && (this.authConcurConf.containsKey(platform))) {
            num = ((Integer)this.authConcurConf.get(platform)).intValue();
        }
        return this.userAuthDao.getAuthTokens(uid, platform, domain, num);
    }

    public UserAuthToken getAuthTokenByUkey(int uid, String ukey)
    {
        return this.userAuthDao.getAuthTokenByUkey(uid, ukey);
    }

    public UserAuthToken parseUkey(String ukey)
    {
        try
        {
            if (ukey.startsWith(UKEY_PREFIX))
            {
                String checkCode = ukey.substring(ukey.length() - CHECK_CODE_BITS);
                String encryptString = ukey.substring(UKEY_PREFIX.length(), ukey.length() - checkCode.length());
                String s = AesUtils.decryptString(encryptString, checkCode);
                if (s.startsWith(UKEY_PREFIX))
                {
                    UserAuthToken token = new UserAuthToken();
                    int segmentLen = SEGMENT_LEN_FORMAT.length();
                    int pos = UKEY_PREFIX.length();

                    int len = Integer.parseInt(s.substring(pos, pos + segmentLen));
                    pos += segmentLen;
                    String segment = s.substring(pos, pos + len);
                    token.setUid(Integer.valueOf(Integer.parseInt(segment)));
                    pos += len;


                    len = Integer.parseInt(s.substring(pos, pos + segmentLen));
                    pos += segmentLen;
                    segment = s.substring(pos, pos + len);
                    token.setDeviceId(segment);
                    pos += len;


                    len = Integer.parseInt(s.substring(pos, pos + segmentLen));
                    pos += segmentLen;
                    segment = s.substring(pos, pos + len);
                    Date expireTime = new Date(Long.parseLong(segment));
                    token.setExpireTime(expireTime);
                    pos += len;

                    len = Integer.parseInt(s.substring(pos, pos + segmentLen));
                    pos += segmentLen;
                    segment = s.substring(pos, pos + len);
                    token.setIp(segment);
                    pos += len;

                    len = Integer.parseInt(s.substring(pos, pos + segmentLen));
                    pos += segmentLen;
                    segment = s.substring(pos, pos + len);
                    Date createTime = new Date(Long.parseLong(segment));
                    token.setCreateTime(createTime);
                    pos += len;

                    len = SALT_BITS;
                    String salt = s.substring(pos, pos + len);
                    pos += len;

                    len = CHECK_CODE_BITS;
                    String checkCode2 = s.substring(pos, pos + len);
                    if (checkCode.equals(checkCode)) {
                        return token;
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private String createUkey(UserAuthToken token)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(UKEY_PREFIX);
        DecimalFormat df = new DecimalFormat(SEGMENT_LEN_FORMAT);

        String segment = String.valueOf(token.getUid());
        sb.append(df.format(segment.length()));
        sb.append(segment);

        segment = token.getDeviceId() == null ? "" : DigestUtils.sha256Hex(String.valueOf(token.getDeviceId()));
        sb.append(df.format(segment.length()));
        sb.append(segment);

        segment = String.valueOf(token.getExpireTime().getTime());
        sb.append(df.format(segment.length()));
        sb.append(segment);

        segment = token.getIp() == null ? "" : String.valueOf(token.getIp());
        sb.append(df.format(segment.length()));
        sb.append(segment);

        segment = String.valueOf(token.getCreateTime().getTime());
        sb.append(df.format(segment.length()));
        sb.append(segment);

        Random r = new Random();
        for (int i = 0; i < SALT_BITS; i++) {
            sb.append(r.nextInt(10));
        }
        String shaString = DigestUtils.sha256Hex(sb.toString());
        String checkCode = shaString.substring(shaString.length() - CHECK_CODE_BITS);
        sb.append(checkCode);
        String ukey = UKEY_PREFIX + AesUtils.encryptString(sb.toString(), checkCode) + checkCode;
        return ukey;
    }

    private String getAuthTokenPlatformByDeviceId(String deviceId)
    {
        if (deviceId.contains("Mozilla")) {
            return "browser";
        }
        return "browser";
    }

    public UserAuthToken createUserAuthToken(int uid, String domain, String platform, String deviceName, String deviceId, String ip)
    {
        if ((deviceName == null) && (deviceId != null)) {
            deviceName = StringUtils.getBrowserName(deviceId);
        }
        UserAuthToken token = new UserAuthToken();
        token.setDomain(domain);
        token.setType(platform);
        token.setUid(Integer.valueOf(uid));
        token.setDeviceName(deviceName);
        token.setDeviceId(deviceId);
        token.setStatus(UserAuthToken.STATUS_LOGIN);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(13, this.expireTimeInSeconds);
        token.setExpireTime(calendar.getTime());
        token.setCreateTime(new Date());
        token.setUkey(createUkey(token));
        token.setIp(ip);
        addAuthToken(token);
        return token;
    }

    public UserAuthToken createUserAuthToken(int uid, String domain, HttpServletRequest request)
    {
        String userAgent = request.getHeader("user-agent");
        String deviceName = StringUtils.getBrowserName(userAgent);
        String ip = NetworkUtils.getIpAddress(request);
        String platform = getContextParam(request, "platform");
        if (platform == null) {
            platform = getAuthTokenPlatformByDeviceId(userAgent);
        }
        return createUserAuthToken(uid, domain, platform, deviceName, userAgent, ip);
    }

    public UserAuthToken createUserAuthToken(int uid, HttpServletRequest request)
    {
        String domain = request.getHeader("host");
        if(domain.indexOf(":")>0){
            domain = domain.substring(0,domain.indexOf(":"));
        }
        return createUserAuthToken(uid, domain, request);
    }

    public UserAuthToken getCheckedAuthToken(String ukey, String domain, String platform, String deviceName, String deviceId, String ip)
    {
        UserAuthToken token = parseUkey(ukey);
        this.logger.debug("=====AUTH===== parse ukey:" + token);
        if (token != null)
        {
            String deviceIdCheckedString = deviceId == null ? "" : DigestUtils.sha256Hex(String.valueOf(deviceId));

            List<UserAuthToken> tokens = getAuthTokens(token.getUid().intValue(), domain, platform);
            if ((tokens != null) && (tokens.size() > 0)) {
                for (int i = 0; i < tokens.size(); i++)
                {
                    UserAuthToken t = (UserAuthToken)tokens.get(i);
                    this.logger.debug("=====AUTH===== current token:" + t.getUkey());
                    if (t.getUkey().equals(ukey)) {
                        return t;
                    }
                }
            }
        }
        return null;
    }

    public UserAuthToken getCheckedAuthToken(String ukey, String domain, HttpServletRequest request)
    {
        String userAgent = request.getHeader("user-agent");
        String deviceName = StringUtils.getBrowserName(userAgent);
        String ip = NetworkUtils.getIpAddress(request);
        String platform = getContextParam(request, "platform");
        if (platform == null) {
            platform = getAuthTokenPlatformByDeviceId(userAgent);
        }
        return getCheckedAuthToken(ukey, domain, platform, deviceName, userAgent, ip);
    }

    public UserAuthToken getCheckedAuthToken(String ukey, HttpServletRequest request)
    {
        String domain = request.getHeader("host");
        if(domain.indexOf(":")>0){
            domain = domain.substring(0,domain.indexOf(":"));
        }
        return getCheckedAuthToken(ukey, domain, request);
    }

    public UserAuthToken getCheckedAuthToken(HttpServletRequest request)
    {
        String ukey = getContextParam(request, CookieKey.UKEY);
        String domain = request.getHeader("host");
        if(domain.indexOf(":")>0){
            domain = domain.substring(0,domain.indexOf(":"));
        }
        return getCheckedAuthToken(ukey, domain, request);
    }

    private String getContextParam(HttpServletRequest request, String k)
    {
        try
        {
            String v = request.getHeader(k);
            if (v == null)
            {
                Cookie cookie = CookieUtils.getCookie(request, k);
                if (cookie != null) {
                    v = cookie.getValue();
                }
            }
            if (v == null) {
                v = request.getParameter(k);
            }
            return v;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public UserAuthToken getCheckedCookieAuthToken(HttpServletRequest request)
    {
        String ukey = getContextParam(request, CookieKey.UKEY);
        Integer uid = Utils.parseInt(getContextParam(request, CookieKey.UID));
        UserAuthToken token = getCheckedAuthToken(ukey, request);
        if ((token != null) && (uid != null) && (uid.equals(token.getUid()))) {
            return token;
        }
        return null;
    }

    public void loginWithCookie(UserAuthToken token, HttpServletResponse response)
    {
        int cookieMaxAge = (int)(token.getExpireTime().getTime() / 1000L - new Date().getTime() / 1000L);
        CookieUtils.addCookie(response, CookieKey.UKEY, token.getUkey(), token.getDomain(), null, cookieMaxAge);
        CookieUtils.addCookie(response, CookieKey.UID, String.valueOf(token.getUid()), token.getDomain(), null, cookieMaxAge);
    }

    public void logoutWithCookie(UserAuthToken token, HttpServletResponse response)
    {
        token.setExpireTime(new Date());
        token.setStatus(UserAuthToken.STATUS_LOGOUT);
        updateAuthToken(token);
        CookieUtils.removeCookie(response, CookieKey.UKEY, token.getDomain(), null);
        CookieUtils.removeCookie(response, CookieKey.UID, token.getDomain(), null);
    }

    public void refeshToken(UserAuthToken token, HttpServletResponse response)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(13, this.expireTimeInSeconds);
        token.setExpireTime(calendar.getTime());
        token.setUkey(createUkey(token));

        UserAuthToken newToken = new UserAuthToken();
        newToken.setUid(token.getUid());
        newToken.setExpireTime(token.getExpireTime());
        newToken.setUkey(token.getUkey());

        updateAuthToken(token);
    }
}
