//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.auth.service;

import com.yiju.web.auth.model.UserAuthToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface UserAuthService {
    int addAuthToken(UserAuthToken var1);

    boolean updateAuthToken(UserAuthToken var1);

    List<UserAuthToken> getAuthTokens(int var1);

    List<UserAuthToken> getAuthTokens(int var1, String var2);

    List<UserAuthToken> getAuthTokens(int var1, String var2, String var3);

    UserAuthToken getAuthTokenByUkey(int var1, String var2);

    UserAuthToken parseUkey(String var1);

    UserAuthToken createUserAuthToken(int var1, String var2, String var3, String var4, String var5, String var6);

    UserAuthToken createUserAuthToken(int var1, String var2, HttpServletRequest var3);

    UserAuthToken createUserAuthToken(int var1, HttpServletRequest var2);

    UserAuthToken getCheckedAuthToken(String var1, String var2, String var3, String var4, String var5, String var6);

    UserAuthToken getCheckedAuthToken(String var1, String var2, HttpServletRequest var3);

    UserAuthToken getCheckedAuthToken(String var1, HttpServletRequest var2);

    UserAuthToken getCheckedAuthToken(HttpServletRequest var1);

    UserAuthToken getCheckedCookieAuthToken(HttpServletRequest var1);

    void loginWithCookie(UserAuthToken var1, HttpServletResponse var2);

    void logoutWithCookie(UserAuthToken var1, HttpServletResponse var2);

    void refeshToken(UserAuthToken var1, HttpServletResponse var2);
}
