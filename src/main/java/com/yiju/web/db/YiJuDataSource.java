//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yiju.web.db;

import com.yiju.web.utils.encrypt.YiJuEncrypt;
import org.apache.commons.dbcp.BasicDataSource;

public class YiJuDataSource extends BasicDataSource {
    public YiJuDataSource() {
    }

    public void setPassword(String password) {
        String c = YiJuEncrypt.decrypt(password);
        if (c == null) {
            c = password;
        }

        super.setPassword(c);
    }

    public void setUsername(String username) {
        String c = YiJuEncrypt.decrypt(username);
        if (c == null) {
            c = username;
        }

        super.setUsername(c);
    }

    public void setUrl(String url) {
        String c = YiJuEncrypt.decrypt(url);
        if (c == null) {
            c = url;
        }

        super.setUrl(c);
    }

    public void setDriverClassName(String driverClassName) {
        String c = YiJuEncrypt.decrypt(driverClassName);
        if (c == null) {
            c = driverClassName;
        }

        super.setDriverClassName(c);
    }
}
